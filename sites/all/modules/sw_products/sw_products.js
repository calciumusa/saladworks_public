/* sw_products.js */

jQuery.noConflict();
jQuery(document).ready(function($) {

  function checkWidth() {

    function viewport() {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    }

    var windowsize = viewport().width;
    return windowsize;
  }

  var windowsize = checkWidth();

  function fixNutritionPanel() {
    windowsize = checkWidth();
    if ( windowsize > 1023 ) {
        contentSectionHeight = $('.page').height() - $('#global-footer').height();
        if( $('body').scrollTop() > 1100 ) {
          vOffset = $('#global-footer').height() - 260;
          $('.nutrition-wrap').css({'bottom':vOffset + 'px'});
        }
        if( $('body').scrollTop() < $('.header').height() ) {
          $('.nutrition-wrap').css({'bottom':'0px'});
        }

    } else if ( windowsize > 767 ) {
        if( $('body').scrollTop() > 1100 ) {
          vOffset = $('#global-footer').height();
          $('.nutrition-wrap').css({'bottom':vOffset + 'px'});
        }
        if( $('body').scrollTop() < $('.header').height() ) {
          $('.nutrition-wrap').css({'bottom':'0px'});
        }
    }
  }

  $(window).scroll(function(){
    fixNutritionPanel();
  });

  $(window).resize(function(){
    fixNutritionPanel();
  });

/*** on page load, store a session array variable with single ounce soup sizes ***/
  var soupSingleServing = new Array();
  soupSingleServing['calories'] = parseFloat($('#cal_total').html(), 10);
  soupSingleServing['fat'] = parseFloat($('#fat_total').html(), 10);
  soupSingleServing['saturatedFat'] = parseFloat($('#satfat_total').html(), 10);
  soupSingleServing['cholesterol'] = parseFloat($('#chol_total').html(), 10);
  soupSingleServing['sodium'] = parseFloat($('#sodium_total').html(), 10);
  soupSingleServing['carbs'] = parseFloat($('#carb_total').html(), 10);
  soupSingleServing['fiber'] = parseFloat($('#fiber_total').html(), 10);
  soupSingleServing['sugar'] = parseFloat($('#sugar_total').html(), 10);
  soupSingleServing['protein'] = parseFloat($('#protein_total').html(), 10);


  $('#soup-size').change(function() {
      var sizeMultiplier = $('#soup-size').children('option:selected').attr('value');
      var sizeText = $('#soup-size').children('option:selected').text();
      if(sizeText == 'Please Select a Size') {
        sizeText = 'Per Ounce';
      }
      var servingSizeHTML = 'Serving Size ' + sizeText;
      var servingCountHTML = 'Servings Per Container: 1 Serving';

      $('#itemServingSize').html(servingSizeHTML);
      $('#itemServingCount').html(servingCountHTML);

  		/*Calories*/
  		var new_cal = soupSingleServing['calories'] * sizeMultiplier;
  		new_cal = (new_cal).toFixed(2);
  		$('#cal_total').html(new_cal);

  		/*Fat*/
  		var new_fat = soupSingleServing['fat'] * sizeMultiplier;
  		new_fat = (new_fat).toFixed(2);
  		$('#fat_total').html(new_fat);

  		/*Sat Fat*/
  		var new_sat = soupSingleServing['saturatedFat'] * sizeMultiplier;
  		new_sat = (new_sat).toFixed(2);
  		$('#satfat_total').html(new_sat);

  		/*Cholesterol*/
  		var new_chol = soupSingleServing['cholesterol'] * sizeMultiplier;
  		new_chol = (new_chol).toFixed(2);
  		$('#chol_total').html(new_chol);

      /*Sodium*/
      var new_sod = soupSingleServing['sodium'] * sizeMultiplier;
      new_sod = (new_sod).toFixed(2);
      $('#sodium_total').html(new_sod);

      /*Carbohydrate*/
      var new_carb = soupSingleServing['carbs'] * sizeMultiplier;
      new_carb = (new_carb).toFixed(2);
      $('#carb_total').html(new_carb);

      /*Fiber*/
      var new_fiber = soupSingleServing['fiber'] * sizeMultiplier;
      new_fiber = (new_fiber).toFixed(2);
      $('#fiber_total').html(new_fiber);

      /*Sugars*/
      var new_sugar = soupSingleServing['sugar'] * sizeMultiplier;
      new_sugar = (new_sugar).toFixed(2);
      $('#sugar_total').html(new_sugar);

      /*Protein*/
      var new_protein = soupSingleServing['protein'] * sizeMultiplier;
      new_protein = (new_protein).toFixed(2);
      $('#protein_total').html(new_protein);
  });



  $('.cyo-input').change(function(){

    /*Add class 'activated' to label*/
    var this_label = "label[for='"+$(this).attr('id')+"']";
    $(this_label).toggleClass('activated');

    /*Get the ID of the saladworks item*/
  	var this_id = parseInt($(this).attr('sw-id'));
  	var this_item = sw_data[this_id - 1];

  	if ( this.checked ) {

  		/*Calories*/
  		var current_cal = parseFloat($('#cal_total').html(), 10);
  		var new_cal = current_cal + this_item['calories'];
  		new_cal = (new_cal).toFixed(2);
  		$('#cal_total').html(new_cal);

  		/*Fat*/
  		var current_fat = parseFloat($('#fat_total').html(), 10);
  		var new_fat = current_fat + this_item['fat_total'];
  		new_fat = (new_fat).toFixed(2);
  		$('#fat_total').html(new_fat);

  		/*Sat Fat*/
  		var current_sat = parseFloat($('#satfat_total').html(), 10);
  		var new_sat = current_sat + this_item['fat_sat'];
  		new_sat = (new_sat).toFixed(2);
  		$('#satfat_total').html(new_sat);

  		/*Cholesterol*/
  		var current_chol = parseFloat($('#chol_total').html(), 10);
  		var new_chol = current_chol + this_item['cholesterol'];
  		new_chol = (new_chol).toFixed(2);
  		$('#chol_total').html(new_chol);

      /*Sodium*/
      var current_sod = parseFloat($('#sodium_total').html(), 10);
      var new_sod = current_sod + this_item['sodium'];
      new_sod = (new_sod).toFixed(2);
      $('#sodium_total').html(new_sod);

      /*Carbohydrate*/
      var current_carb = parseFloat($('#carb_total').html(), 10);
      var new_carb = current_carb + this_item['carbohydrate'];
      new_carb = (new_carb).toFixed(2);
      $('#carb_total').html(new_carb);

      /*Fiber*/
      var current_fiber = parseFloat($('#fiber_total').html(), 10);
      var new_fiber = current_fiber + this_item['fiber'];
      new_fiber = (new_fiber).toFixed(2);
      $('#fiber_total').html(new_fiber);

      /*Sugars*/
      var current_sugar = parseFloat($('#sugar_total').html(), 10);
      var new_sugar = current_sugar + this_item['sugars'];
      new_sugar = (new_sugar).toFixed(2);
      $('#sugar_total').html(new_sugar);

      /*Protein*/
      var current_protein = parseFloat($('#protein_total').html(), 10);
      var new_protein = current_protein + this_item['protein'];
      new_protein = (new_protein).toFixed(2);
      $('#protein_total').html(new_protein);

  	} else {

  		/*Calories*/
  		var current_cal = parseFloat($('#cal_total').html(), 10);
  		var new_cal = current_cal - this_item['calories'];
  		new_cal = (new_cal).toFixed(2);
  		$('#cal_total').html(new_cal);

  		/*Fat*/
  		var current_fat = parseFloat($('#fat_total').html(), 10);
  		var new_fat = current_fat - this_item['fat_total'];
  		new_fat = (new_fat).toFixed(2);
  		$('#fat_total').html(new_fat);

  		/*Sat Fat*/
  		var current_sat = parseFloat($('#satfat_total').html(), 10);
  		var new_sat = current_sat - this_item['fat_sat'];
  		new_sat = (new_sat).toFixed(2);
  		$('#satfat_total').html(new_sat);

  		/*Cholesterol*/
  		var current_chol = parseFloat($('#chol_total').html(), 10);
  		var new_chol = current_chol - this_item['cholesterol'];
  		new_chol = (new_chol).toFixed(2);
  		$('#chol_total').html(new_chol);

      /*Sodium*/
      var current_sod = parseFloat($('#sodium_total').html(), 10);
      var new_sod = current_sod - this_item['sodium'];
      new_sod = (new_sod).toFixed(2);
      $('#sodium_total').html(new_sod);

      /*Carbohydrate*/
      var current_carb = parseFloat($('#carb_total').html(), 10);
      var new_carb = current_carb - this_item['carbohydrate'];
      new_carb = (new_carb).toFixed(2);
      $('#carb_total').html(new_carb);

      /*Fiber*/
      var current_fiber = parseFloat($('#fiber_total').html(), 10);
      var new_fiber = current_fiber - this_item['fiber'];
      new_fiber = (new_fiber).toFixed(2);
      $('#fiber_total').html(new_fiber);

      /*Sugars*/
      var current_sugar = parseFloat($('#sugar_total').html(), 10);
      var new_sugar = current_sugar - this_item['sugars'];
      new_sugar = (new_sugar).toFixed(2);

      $('#sugar_total').html(new_sugar);

      /*Protein*/
      var current_protein = parseFloat($('#protein_total').html(), 10);
      var new_protein = current_protein - this_item['protein'];
      new_protein = (new_protein).toFixed(2);
      $('#protein_total').html(new_protein);
  	}
  });
});