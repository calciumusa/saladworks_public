
(function ($) {
	$('body').removeClass('no-js');
	$(document).ready(function(){



		/***
		 *Prepare main menu for showtime by adding classes to the li, div, ul, a, and p elements.
		 */

		 function the_franchise_prepare_menu(menu_class) {
			/* First select all of the elements we will need */
			var counter = 1;
			var first_level_li = $('.' + menu_class + ' > ul > li');
			var second_level_li = $('.' + menu_class + ' > ul > li ul > li');
			var first_level_ul = $('.' + menu_class + ' > ul');
			var second_level_ul = $('.' + menu_class + ' > ul > li > div > ul');
			var first_level_div = $('.' + menu_class + ' > ul > li > div');
			var second_level_div = $('.' + menu_class + ' > ul > li ul > li > div');
			var first_level_p = $('.' + menu_class + ' > ul > li > div > p');
			var second_level_p = $('.' + menu_class + ' > ul > li ul > li > div > p');
			var first_level_a = $('.' + menu_class + ' > ul > li > div > a');
			var second_level_a = $('.' + menu_class + ' > ul > li ul > li > div > a');
			/* Now we assign the classes we'll need */
			first_level_ul.addClass('level-1-ul');
			first_level_li.addClass('level-1-li');
			first_level_div.addClass('level-1-div');
			first_level_p.addClass('level-1-p italic fw-900 color-white');
			first_level_a.addClass('level-1-a italic fw-900 color-white');
			second_level_ul.addClass('level-2-ul');
			second_level_li.addClass('level-2-li');
			second_level_a.addClass('level-2-a');
			second_level_p.remove();

			first_level_p.each(function(){
				$(this).html(String(counter));
				counter++;
			});

			$('.' + menu_class).addClass('ready');
		 }

		/***
		 *min height on main container
		 */

		 function set_height_to_menu() {
		 	var the_height = $('.section-menu').height();
		 	the_height = the_height - 20;
		 	$('.main-innerer').css({ "min-height": the_height + "px" })
		 }

		/***
		 *Tour A Saladworks
		 */

		function create_tour_array() {
			var tour_data = [];
			var i = 0;
			$('.thumbnail-nav .bx-viewport li').each(function(){
				var the_id = $(this).find('.id-wrap').attr('id');
				tour_data[the_id] = [];
				tour_data[the_id]['src'] = $(this).find('.tour-thumb-wrap img').attr('src');
				i++;
			});
			return tour_data;
		}

		function take_the_tour(el){
			var the_id = el.attr('id');
			$('.tour-img-wrap img').attr('src', tour_data[the_id]['src']);
		}

		/***
		 *Hamburger Menu Controls
		 */
		 
		function the_hamburger_controls(el) {
			 $('.' + el).click(function(){
			 	$(this).toggleClass('active');
			 	$('#theMainMenu').toggleClass('active-menu');
			 });
		}
		/**
		 *active menu state
		 */

		 function give_active_trail() {
		 	var active_anchor = $('.the-main-menu .active');
		 	active_anchor_lis = active_anchor.parents('li');
		 	active_anchor_lis.addClass('active-trail')
		 }

		/**
		 *Calling functions
		 */
		 /*adjust_footer_position();*/
		 the_franchise_prepare_menu('the-main-menu');
		 the_hamburger_controls('hamburger-wrap');
		 $('body').delay(200).addClass('ready');
		 if ($('body').hasClass('is-photo-tour')){var tour_data = create_tour_array();}
		 $('.id-wrap').click(function(){
		 	take_the_tour($(this));
		 });
		 give_active_trail();
		 set_height_to_menu();
	});
})(jQuery);