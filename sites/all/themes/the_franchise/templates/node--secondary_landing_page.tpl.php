<?php
//kpr(get_defined_vars());
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php

//to remove all markup around a given field call the nomarkup theme function 
//$content['field_name']['#theme'] = "nomarkup";

if ($classes) {
  $classes = ' class="'. $classes . ' "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

hide($content['comments']);
hide($content['links']);
?>

<?php
/*Set all content vars*/
$theme_path = "/sites/all/themes/the_franchise";
$landing_img_url = file_create_url($node->field_featured_landing_page_imag["und"][0]["uri"]);

$buckets = array();
$buckets[1] = array();
$buckets[2] = array();
$buckets[3] = array();
$buckets[1]["has_url"] = false;
$buckets[2]["has_url"] = false;
$buckets[3]["has_url"] = false;
$bucket_count = 0;

if ( count($node->field_bucket_image) == 1 ) {
  $buckets[1]["img"] = file_create_url($node->field_bucket_image["und"][0]["uri"]);
  $buckets[1]["title"] = $node->field_bucket_title["und"][0]["value"];
  $buckets[1]["body"] = $node->field_bucket_body["und"][0]["value"];
  $bucket_count++;
}
if ( count($node->field_bucket_image_2) == 1 ) {
  $buckets[2]["img"] = file_create_url($node->field_bucket_image_2["und"][0]["uri"]);
  $buckets[2]["title"] = $node->field_bucket_title_2["und"][0]["value"];
  $buckets[2]["body"] = $node->field_bucket_body_2["und"][0]["value"];
  $bucket_count++;
}
if ( count($node->field_bucket_image_3) == 1 ) {
  $buckets[3]["img"] = file_create_url($node->field_bucket_image_3["und"][0]["uri"]);
  $buckets[3]["title"] = $node->field_bucket_title_3["und"][0]["value"];
  $buckets[3]["body"] = $node->field_bucket_body_3["und"][0]["value"];
  $bucket_count++;
}

if ( count($node->field_bucket_url) == 1 ) {
  $buckets[1]["url"] = $node->field_bucket_url["und"][0]["value"];
  $buckets[1]["has_url"] = true;
}
if ( count($node->field_bucket_url_2) == 1 ) {
  $buckets[2]["url"] = $node->field_bucket_url_2["und"][0]["value"];
  $buckets[2]["has_url"] = true;
}
if ( count($node->field_bucket_url_3) == 1 ) {
  $buckets[3]["url"] = $node->field_bucket_url_3["und"][0]["value"];
  $buckets[3]["has_url"] = true;
}

$bucket_count_mod = $bucket_count % 4;

if ( $bucket_count_mod == 1 ) {
  $size_class = "one-bucket";
}
if ( $bucket_count_mod == 2 ) {
  $size_class = "two-bucket";
}
if ( $bucket_count_mod == 3 ) {
  $size_class = "three-bucket";
}
if ( $bucket_count_mod == 0 ) {
  $size_class = "four-bucket";
}
?>

<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- node.tpl.php -->
<?php } ?>
<article <?php print $id_node . $classes .  $attributes; ?> role="article">
  <?php print $mothership_poorthemers_helper; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
  <footer>
    <?php print $user_picture; ?>
    <span class="author"><?php print t('Written by'); ?> <?php print $name; ?></span>
    <span class="date"><?php print t('On the'); ?> <time><?php print $date; ?></time></span>

    <?php if(module_exists('comment')): ?>
      <span class="comments"><?php print $comment_count; ?> Comments</span>
    <?php endif; ?>
  </footer>
  <?php endif; ?>

  <div class="content">
    <div class="picture-wrap">
      <div class="secondary-pict-box">
        <div class="pict-skin-3q">
          <img src="<?php echo $landing_img_url; ?>" />
        </div>
      </div>
      <img class="pict-shadow" src="<?php echo $theme_path; ?>/img/homeslider-undshadow.png">
    </div>

    <?php if($bucket_count > 0) { ?>
      <ul class="secondary-bucket-wrap">
        <?php for ($i = 1; $i <= $bucket_count; $i++) { ?>
          <li class="<?php echo $size_class; ?> align-center" id="bucket<?php echo $i; ?>">
            
            <?php if ( $buckets[$i]["has_url"] == true ) { ?>
              <a style="text-decoration: none;" href='<?php echo $buckets[$i]["url"]; ?>'>
            <?php } ?>

              <div class="bucket-img" style='background-image: url("<?php echo $buckets[$i]["img"]; ?>");'></div>
              
              <?php if ( $buckets[$i]["has_url"] == true ) { ?>
                </a>
              <?php } ?>

              <?php if ( $buckets[$i]["has_url"] == true ) { ?>
                <a style="text-decoration: none;" href='<?php echo $buckets[$i]["url"]; ?>'>
              <?php } ?>

              <h3 class="color-white fw-900 fam-header italic"><?php echo $buckets[$i]["title"]; ?></h3>
              
              <?php if ( $buckets[$i]["has_url"] == true ) { ?>
                </a>
              <?php } ?>

            <p class="color-white fw-500 fam-body"><?php echo $buckets[$i]["body"]; ?></p>
          </li>
        <?php } ?>
      </ul>
    <?php } ?>

  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
</article>

<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- /node.tpl.php -->
<?php } ?>