
(function ($) {

	$(document).ready(function(){

				/***
		 *soup breadcrumbs
		 */

		 function soup_breadcrumb() {
		 	$('.easy-breadcrumb_segment-1').remove();
		 	$('.easy-breadcrumb_segment-separator:first').remove();
		 }

		/***
		 *Prepare main menu for showtime by adding classes to the li, div, ul, a, and p elements.
		 */

		 function the_franchise_prepare_menu(menu_class) {

			/* First select all of the elements we will need */
			var first_level_li = $('.' + menu_class + ' > ul > li');
			var first_level_ul = $('.' + menu_class + ' > ul');
			var first_level_a = $('.' + menu_class + ' > ul > li > a');
			/* Now we assign the classes we'll need */
			first_level_ul.addClass('level-1-ul');
			first_level_li.addClass('level-1-li');
			first_level_a.addClass('level-1-a fw-900 color-white');
			$('.' + menu_class).addClass('ready');
		 } /* the_franchise_prepare_menu() */

		/***
		 *Hamburger Menu Controls
		 */

		function the_hamburger_controls(el) {
			 $('.' + el).click(function(){
			 	$(this).toggleClass('active');
			 	$('.header-main-menu').toggleClass('active');
			 	$('body').toggleClass('active-menu');
			 });
		} /* the_hamburger_controls */



		function the_callout_colors(){
			var color_1 = $('#holds-color-1').attr('class');
			var color_2 = $('#holds-color-2').attr('class');
			var color_3 = $('#holds-color-3').attr('class');

			$('.home-page-callout h2').attr('style', 'color: '+ color_1 + ';')
			$('.home-page-callout h3').attr('style', 'color: '+ color_1 + ';')
			$('.home-page-callout .callout-body').attr('style', 'color: '+ color_2 + ';')
			$('.home-page-callout .callout-learn-more').attr('style', 'color: '+ color_3 + ';')
			$('.home-page-callout').addClass('ready');
		} /* the_callout_colors */

		function zip_me_away(){
			/* this is a nice function.  just saying. */
			var input = escape( $('#ot_zip').val() );
			var valid = '0123456789-';
			var hyphencount = 0;
			if ( input.length != 5 && input.length != 10) {
				alert( "Please enter your 5 digit or 5 digit+4 zip code." );
				return false;
			}

			for ( var i=0; i < input.length; i++ ) {
				temp = "" + input.substring(i, i+1);
				if ( temp == "-" ) hyphencount++;
				if ( valid.indexOf(temp) == "-1" ) {
					alert( "Invalid characters in your zip code.  Please try again." );
					return false;
				}

				if ( (hyphencount > 1) || ((input.length==10) && ""+input.charAt(5)!="-") ) {
					alert( "The hyphen character should be used with a properly formatted 5 digit+four zip code, like '12345-6789'.   Please try again." );
					return false;
				}
			}

			var url_string = 'https://online.saladworks.com/#/zipcoderesults/';
			url_string += input;

			var win=window.open(url_string, '_blank');
		  	win.focus();
		} /* zip_me_away */


		function sandwich_switch(el){
			var that = $(el);
			var switch_to = that.attr('sand-type');
			$('.active').removeClass('active');
			that.addClass('active');
			$('.menu-block').addClass('hidden');
			$("." + switch_to).removeClass('hidden');
		}

		if ( logged_in ===false ) {
			$('#customer-pane .fieldset-wrapper .fieldset-description').html('Enter a valid email address for this order.');
		}

		if ( $('body').hasClass('page-cart-checkout')) {
			$('.grippie').remove();
			$('.payment-details-credit').prepend('<span class="cred-info fieldset-legend">Credit Card Information</span>')
			var commentPaneHTML = '<legend><span class="fieldset-legend">Gift Message (optional)</span></legend><div class="fieldset-wrapper"><div class="fieldset-description">If you want to include a custom gift message, please specify the message here.</div><div><label for="edit-panes-comments-comments">Order comments</label><div class="form-textarea-wrapper resizable textarea-processed resizable-textarea"><textarea id="edit-panes-comments-comments" name="panes[comments][comments]" cols="60" rows="5"></textarea></div></div></div>';
			$('#comments-pane').html(commentPaneHTML);
		}


		/**
		 *  Dynamically adjust height of main content area if page body < viewport so footer is at bottom.
		 */
    function adjust_footer_position() {
      var viewportHeight = $(window).height();
      $('.page').css({'min-height': '0px','height':'auto'});
      var contentHeight = $('body').height();
      var contentPaddingTop = 120;
      var innerFooterHeight = $('#inner-footer').height();
      if (viewportHeight > contentHeight) {
        $('.page').css({'min-height': (viewportHeight - contentHeight - contentPaddingTop - innerFooterHeight) + contentHeight + 'px'});
      }
    } /* adjust_footer_position */



		/**
		 *  Sniff for ubercart gift card product page class identifier and swap text box for select
		 */
    if( $('body').hasClass('node-type-gift-card') && $('body').hasClass('uc-product-node') ) {
      var selectHTML = '<select id="edit-varprice--2" name="varprice">' + "\n";
      selectHTML += '<option value="select" selected="selected"> -Select Value- </option>' + "\n";
      selectHTML += '<option value="10.00">$10.00</option>' + "\n";
      selectHTML += '<option value="20.00">$20.00</option>' + "\n";
      selectHTML += '<option value="30.00">$30.00</option>' + "\n";
      selectHTML += '<option value="40.00">$40.00</option>' + "\n";
      selectHTML += '<option value="50.00">$50.00</option>' + "\n";
      selectHTML += '<option value="60.00">$60.00</option>' + "\n";
      selectHTML += '<option value="70.00">$70.00</option>' + "\n";
      selectHTML += '<option value="80.00">$80.00</option>' + "\n";
      selectHTML += '<option value="90.00">$90.00</option>' + "\n";
      selectHTML += '<option value="100.00">$100.00</option>' + "\n";
      selectHTML += '</select>' + "\n";
      $(selectHTML).insertBefore( $('form').children('div').children('#edit-varprice--2') );
      $('form').children('div').children('input[type="text"]#edit-varprice--2').remove();
      $('form').children('div').children('input[type="text"]#edit-varprice--2').remove();

    } /* end if statement */

    if( $('body').hasClass('page-cart') ) {
      $('#uc-cart-view-form').children('table.tableheader-processed').children('tbody').children('tr:last').addClass('summaryRow');
      $('td.image a img').each(function(){
      	  var src_thumb = $(this).attr('src');
	      var st_start_pos = src_thumb.indexOf("public/") + 7;
	      var st_end_pos = src_thumb.indexOf("g?") + 1;
	      src_thumb = src_thumb.substring(st_start_pos, st_end_pos);
	      src_thumb = "/sites/default/files/" + src_thumb;
	      $(this).attr('src', src_thumb);
      });
    } /* end cart page adjustments */


    $(window).resize(function () {
      adjust_footer_position();
    });


    /**
    *  On scroll functions/callbacks
    *  If mobile menu is open/displayed, scrolling background page toggles it off
    */
    $(window).scroll(function() {
      if( $('.header-main-menu').hasClass('active') ) {
        $('.header-main-menu').removeClass('active');
        $('.hamburger-wrap').removeClass('active');
        $('body').removeClass('active-menu');
      }
    });




		/**
		 *Calling functions on document ready
		 */
    	adjust_footer_position();
		the_callout_colors();
		the_franchise_prepare_menu('header-main-menu');
		the_hamburger_controls('hamburger-wrap');
		/*$('.header-main-menu').swiperight(function(){
		 	$('.hamburger-wrap').toggleClass('active');
		 	$('.header-main-menu').toggleClass('active');
		 	$('body').toggleClass('active-menu');
		});*/
		$('#ot_submit').click(function(){
		 	zip_me_away();
		});
		$('#allergens').click(function(){
			window.open('http://gipsee.com/saladworks/','name','height=600,width=350,scrollbars=yes,resizable=yes');
			return false;
		});
		$('.sandwich-link a').click(function(){
			sandwich_switch(this);
		});
		if ($('body').hasClass('soup-landing')) {
		 	soup_breadcrumb();
		}

	}); /* $(document).ready() */

})(jQuery);