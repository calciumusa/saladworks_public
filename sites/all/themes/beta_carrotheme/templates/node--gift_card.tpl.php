<?php
//kpr(get_defined_vars());
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php

//to remove all markup around a given field call the nomarkup theme function
//$content['field_name']['#theme'] = "nomarkup";

$node_wrapper = entity_metadata_wrapper('node',$node);
//  dpm( $node_wrapper->value() );
    $productDescription = $node_wrapper->body->value->value();
    $designSku = $node_wrapper->model->value();

    $productImage = $node_wrapper->uc_product_image->value();
//    dpm($productImage[0]);

    $productImageAltText = $productImage[0]['alt'];
    $productImageFilename = $productImage[0]['filename'];
//    dpm($productImageAltText);
//    dpm($productImageFilename);


if ($classes) {
  $classes = ' class="content ubercart-product '. $classes . ' "';
} else {
  $classes = ' class="content ubercart-product "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

hide($content['links']);
?>

<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- node.tpl.php -->
<?php } ?>
<article <?php print $id_node . $classes .  $attributes; ?> role="article">
  <?php print $mothership_poorthemers_helper; ?>

    <div class="giftcardImage">
      <h2<?php print $title_attributes; ?>>Giftcard Design: <?php print $title; ?></h2>
      <img src="/sites/default/files/<?php echo $productImageFilename;?>" alt="<?php echo $productImageAltText;?>" />
    </div><!--/.giftcardImage-->

    <div class="productInfo">
      <h2<?php print $title_attributes; ?>>Design: <?php print $title; ?></h2>
      <div class="add-to-cart block-wrapper">
        <?php
          $thisID =  $node->nid;
          $addToCartForm = drupal_get_form('uc_product_add_to_cart_form_'.$thisID, $node);
          $contentHTML = drupal_render($addToCartForm);
          $contentHTML = str_replace('<span class="field-prefix">$</span>','',$contentHTML); /* remove $ prefixing select */
          $contentHTML = str_replace ('<div>','<div class="field-wrapper">', $contentHTML);
          $contentHTML = str_replace ('</small>','<br><br>If you wish to purchase a gift card for more than $100.00, please call 610-825-3080</small>', $contentHTML);
          print $contentHTML;
        ?>
        <?php print render($content['links']); ?>
      </div><!--/.block-wrapper-->
    </div><!--/.productInfo-->

</article><!--/.ubercart-product-->


<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- /node.tpl.php -->
<?php } ?>
