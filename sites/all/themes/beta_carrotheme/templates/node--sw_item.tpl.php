<?php
//kpr(get_defined_vars());
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php

//to remove all markup around a given field call the nomarkup theme function
//$content['field_name']['#theme'] = "nomarkup";

if ( array_key_exists('und', $field_saladworks_id) ) {
  $node_sw_id = $field_saladworks_id['und'][0]['value'];
} else {
  $node_sw_id = $field_saladworks_id[0]['value'];
}

$sw_item = array();
$results = db_select('sw_products','p')
 ->fields('p',array('sw_products_id','sw_products_sku','sw_products_name','sw_products_category','last_modified','sw_products_body','sw_products_thumb','sw_products_image','sw_products_serv_size','sw_products_serv_per','sw_products_calories','sw_products_fat_total','sw_products_fat_sat','sw_products_cholesterol','sw_products_sodium','sw_products_carbohydrate','sw_products_fiber','sw_products_sugars','sw_products_protein'))
 ->orderBy('sw_products_name', 'ASC')
 ->execute();

foreach ($results as $result) {
  if ( $node_sw_id ) {
    if( $result->sw_products_id == $node_sw_id ) {
      $sw_item['name'] = $result->sw_products_name;
      $sw_item['id'] = $result->sw_products_id;
      $sw_item['thumb'] = $result->sw_products_thumb;
      $sw_item['image'] = $result->sw_products_image;
      $sw_item['sku'] = $result->sw_products_sku;
      $sw_item['tid'] = $result->sw_products_category;
      $sw_item['term'] = taxonomy_term_load($sw_item['tid']);
      $sw_item['cat'] = $sw_item['term']->name;
      $sw_item['lastModified'] = $result->last_modified;
      if ( array_key_exists('und', $body) ) {
        $sw_item['body'] = $body['und'][0]['value'];
      } else if ( array_key_exists(0, $body) ) {
        $sw_item['body'] = $body[0]['value'];
      } else {
        $sw_item['body'] = "";
      }
      if ( array_key_exists('und', $body) ) {
        $sw_item['summary'] = $body['und'][0]['summary'];
      } else if ( array_key_exists(0, $body) ) {
        $sw_item['summary'] = $body[0]['summary'];
      } else {
        $sw_item['summary'] = "";
      }
      $sw_item['serv'] = $result->sw_products_serv_size;
      $sw_item['per'] = $result->sw_products_serv_per;
      $sw_item['cal'] = $result->sw_products_calories;
      $sw_item['fat_total'] = $result->sw_products_fat_total;
      $sw_item['fat_sat'] = $result->sw_products_fat_sat;
      $sw_item['chol'] = $result->sw_products_cholesterol;
      $sw_item['sodium'] = $result->sw_products_sodium;
      $sw_item['carb'] = $result->sw_products_carbohydrate;
      $sw_item['fiber'] = $result->sw_products_fiber;
      $sw_item['sugars'] = $result->sw_products_sugars;
      $sw_item['protein'] = $result->sw_products_protein;
    }
  }
}

watchdog("sw_products", '<pre>' . print_r( $sw_item, true) . '</pre>');

if ($classes) {
  $classes = ' class="'. $classes . ' "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

hide($content['comments']);
hide($content['links']);
?>
<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- node.tpl.php -->
<?php } ?>
<article <?php print $id_node . $classes .  $attributes; ?> role="article">
  <?php print $mothership_poorthemers_helper; ?>

  <!-- BEGIN CUSTOM TPL MARKUP -->
  <div class='salad-wrap'>
    <img class='salad-img' src='<?php echo $sw_item["image"]; ?>' />
    <p class='salad-text'><?php echo $sw_item["body"]; ?></p>
    <div class="fake-button"><a class="button" id="submitOrderTalk" href='https://online.saladworks.com/#/?order=<?php echo $sw_item["sku"]; ?>'>Order Now!</a></div>
  </div>
  <div class='nutrition-wrap'>
    <table class="nutrition_board">
      <tbody class="inner-table">
        <tr>
          <td class="style5" style="font-size: 12px" nowrap="nowrap"><strong>Nutrition Facts</strong></td>
        </tr>
        <tr>
          <td id="itemServingSize" class="style9">Serving Size <?php echo $sw_item["serv"]; ?></td>
        </tr>
        <tr>
          <td id="itemServingCount" class="style9">Servings Per Container <?php echo $sw_item["per"]; ?></td>
        </tr>
        <tr>
          <td class='black-bar eight' style="background:black;"></td>
        </tr>
        <tr>
          <td class="style8"><strong>Amount Per Serving</strong></td>
        </tr>
        <tr>
          <td class="style8">
            <table style="width: 100%">
              <tbody>
                <tr>
                  <td>Calories <span id="cal_total"><?php echo $sw_item["cal"]; ?></span></td>
                  <td class="style1">&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td class='black-bar five' style="background:black;"></td>
        </tr>
        <tr>
          <td class="style8">
            <table style="width: 100%">
              <tbody>
                <tr>
                  <td><strong>Total Fat</strong>&nbsp;<span id="fat_total"><?php echo $sw_item["fat_total"]; ?></span>g</td>
                  <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                  <td style="padding-left:10px;">Saturated Fat&nbsp;<span id="satfat_total"><?php echo $sw_item["fat_sat"]; ?></span>g</td>
                  <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                  <td style="padding-left:10px;"><em>Trans</em> Fat 0.00g</td>
                  <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                  <td><strong>Cholesterol</strong> <span id="chol_total"><?php echo $sw_item["chol"]; ?></span>mg</td>
                  <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                  <td><strong>Sodium</strong> <span id="sodium_total"><?php echo $sw_item["sodium"]; ?></span>mg</td>
                  <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                  <td><strong>Total Carbohydrate</strong> <span id="carb_total"><?php echo $sw_item["carb"]; ?></span>g</td>
                  <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                  <td style="padding-left:10px;">Dietary Fiber <span id="fiber_total"><?php echo $sw_item["fiber"]; ?></span>g</td>
                  <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                  <td style="padding-left:10px;">Sugars <span id="sugar_total"><?php echo $sw_item["sugars"]; ?></span>g</td>
                  <td class="style1">&nbsp;</td>
                </tr>
                <tr>
                  <td><strong>Protein</strong> <span id="protein_total"><?php echo $sw_item["protein"]; ?></span>g</td>
                  <td class="style1">&nbsp;</td>
                </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>

  <p class='disclaimer'>Information analysis performed utilizing the USDA National Nutrient Database for Standard Reference based on Saladworks approved ingredients and recipes. The information listed here is based on standard recipes and product formulations, however, variations may occur based upon offerings at individual locations.</p>
  </div>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content">
    <?php print render($content);?>
  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
</article>

<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- /node.tpl.php -->
<?php } ?>
