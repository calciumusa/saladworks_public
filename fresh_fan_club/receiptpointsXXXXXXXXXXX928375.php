<?php

if(isset($_POST['submit']))
{
	require_once('../swirl/libr.php');

	$amount = preg_replace('/[^\d\.]/','',$_POST['amount']);
	$numPointsToAdd = round($amount / 0.125,0); // 1 point per 12 1/2 cents
	$dailyLimit = 800; //points
	$inDollarsTheLimitIs = 100;

	$today = date("Y-m-d");
	$tomorrow = date("Y-m-d",strtotime("tomorrow"));

	$cardnum = preg_replace('/\D/','',$_POST['card']);
	$receipt = preg_replace('/\D/','',$_POST['receipt']);
	$store = $_POST['store'];
	$tdate = preg_replace('/[^\d\-]/','',$_POST['receiptdate']);
	$tdate2 = date("Y-m-d",strtotime($tdate) + 60 * 60 * 24);

	$message = '';


	if($cardnum == '' || $receipt == '' || $amount == '' || $store == '')
	{
		$message = "All fields are required";
	}
	elseif($amount > $inDollarsTheLimitIs)
	{
		$message = "We�re sorry, the amount of this transaction puts you over our daily limit for points.  Please fax us your receipt for further verification (610-825-3280).";
	}
	else
	{
		$mslink = mssql_connect(HARVEST_HOST,HARVEST_USER,HARVEST_PASS);
		mssql_select_db('Loyalty', $mslink);


		// check account exists in harvest
		$sql = "SELECT COUNT(*) as accts FROM Accounts WHERE AccountNumber = '$cardnum'";
		$res = mssql_query ( $sql, $mslink );
		$row = mssql_fetch_assoc($res);

		if($row['accts'] == 0)
		{
			$message = 'It looks like your card has not yet been registered.  <a href="/fresh_fan_club2/Register">Click here</a> to sign up or <a href="/fresh_fan_club2/Account">click here</a> to add this card to your account';
		}
		else
		{
			// check already credited points either receipt date or today
			$sql = "SELECT Points
					FROM TransactionLog t
					WHERE AccountNumber = '$cardnum'
					  AND RefNo = '$receipt'
					  AND (
							( TransactionDateTime > '$tdate' AND TransactionDateTime < '$tdate2' )
							OR
							( TransactionDateTime > '$today' AND TransactionDateTime < '$tomorrow' )
						  )

					";
			$res = mssql_query ( $sql, $mslink );
			$row = mssql_fetch_assoc($res);

			if($row['Points'])
			{
				$message = "We're sorry, it appears you have already received loyalty points for this transaction.  Please fax us your receipt for further verification (610-825-3280)";
			}
			else
			{
				// check daily limit
				$sql = "SELECT sum(Points) as todayPoints
						 FROM TransactionLog t
						WHERE AccountNumber = '$cardnum'
						  AND TransactionDateTime > '$today'
						  AND TransactionDateTime < '$tomorrow'
						";
				$res = mssql_query ( $sql, $mslink );
				$row = mssql_fetch_assoc($res);
				$todayPointsSubmitted = $row['todayPoints'];

				if($todayPointsSubmitted > $dailyLimit || ($todayPointsSubmitted + $numPointsToAdd > $dailyLimit))
				{
					$message = "We�re sorry, the amount of this transaction puts you over our daily limit for points.  Please fax us your receipt for further verification (610-825-3280).";
				}
				else
				{
					// all good, apply the points
					// The Two Queries
					$mssql = "update accounts set points = points + $numPointsToAdd where accountnumber = '$cardnum'";

					$mssql2 = "INSERT INTO TransactionLog
						   ([AccountID]
						   ,[MerchantID]
						   ,[AccountNumber]
						   ,[MerchantPOSID]
						   ,[RefNo]
						   ,[TranCode]
						   ,[Points]
						   ,[Amount]
						   ,[TransactionDateTime]
						   ,[ReturnCode]
						   ,[AddPoints]
						   ,[TransactionLocalTimeOffset])
					 VALUES
						   ((SELECT AccountID FROM Accounts WHERE Accounts.AccountNumber = '$cardnum')
						   ,353
						   ,'$cardnum'
						   ,'488881'
						   ,'$receipt'
						   ,'Add'
						   ,$numPointsToAdd
						   ,$amount
						   ,getdate()
						   ,'000000'
						   ,$numPointsToAdd
						   ,0)";

					$r = mssql_query ( $mssql, $mslink );
					$r = mssql_query ( $mssql2, $mslink );

					$message = "Congratulations, $numPointsToAdd points have been awarded to your fan card for this transaction.  Please remember to bring your card with you on your next visit to Saladworks";
					$cardnum = '';
					$receipt = '';
					$amount = '';
					$store = '';
				}
			}
		}

		mssql_close($mslink);
	}
}


// 30-day dynamic date drop-down
$timestamp = time();
$receiptDateOptions = '';
for($i=0;$i<30;$i++)
{
	$receiptDateOptions .= '<option value="' . $mynewdate = strftime('%Y-%m-%d',$timestamp) . '"';
	if($i == 0)
	{
		$receiptDateOptions .= ' SELECTED selected="true"';
	}
	$receiptDateOptions .= '>' . strftime('%m-%d-%Y',$timestamp) . '</option>';
	$timestamp = strtotime("-1 days",$timestamp);
}



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Saladworks Fresh Fan Club Points</title>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="/css/saladworks-new.css" />
<link rel="stylesheet" type="text/css" href="/css/sitemap.css" />
<link rel="stylesheet" type="text/css" href="/fresh_fan_club2/css/loyalty.css"/>
<script type="text/javascript" src="/fresh_fan_club2/java/jquery.js"></script>
<script type="text/javascript" src="/fresh_fan_club2/java/loyalty.js"></script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>

<script type="text/javascript">stLight.options({publisher:'4d7b6952-6a0b-416f-ad5f-90fdd53791f5'});</script>

</head><body>
<div id="container">
  <div id="header">
    <div id="right_head">&nbsp;</div>
    <div><a href="http://www.saladworks.com/"><img id="imagehead" alt="Saladworks - Salad Franchise" src="http://www.saladworks.com/images/ds_logo.png" width="142" height="130" /></a> <img style="margin-bottom: 35px" alt="Saladworks - America's Best Salads" src="http://www.saladworks.com/images/abs_words.png" width="542" height="66" /></div>
  </div>
  <div align="center"></div>
  <div id="nav">

    <ul id="navlist">
      <li><a href="http://www.saladworks.com/order_online">order online</a></li>
      <li class="div"><img src="http://www.saladworks.com/images/square/div.jpg" alt="Saladworks-Fantically Fresh" width="1" height="22" /></li>
      <li><a href="http://www.saladworks.com/currentmenu">menu</a></li>
      <li class="div"><img src="http://www.saladworks.com/images/square/div.jpg" alt="Saladworks-Fantically Fresh" width="1" height="22" /></li>
      <li><a href="http://nutrition.saladworks.com/nutrition">nutrition</a></li>
      <li class="div"><img src="http://www.saladworks.com/images/square/div.jpg" alt="Saladworks-Fantically Fresh" width="1" height="22" /></li>

      <li><a href="http://www.saladworks.com/locations">locations</a></li>
      <li class="div"><img src="http://www.saladworks.com/images/square/div.jpg" alt="Saladworks-Fantically Fresh" width="1" height="22" /></li>
      <li><a href="http://www.saladworks.com/own_a_franchise">salad franchise</a></li>
      <li class="div"><img src="http://www.saladworks.com/images/square/div.jpg" alt="Saladworks-Fantically Fresh" width="1" height="22" /></li>
      <li><a href="http://www.saladworks.com/about_us">about us</a></li>
      <li class="div"><img src="http://www.saladworks.com/images/square/div.jpg" alt="Saladworks-Fantically Fresh" width="1" height="22" /></li>
      <li><a href="/gift_cards">gift cards</a></li>

    </ul>
  </div>
  <div class="marquee">
    <marquee>
    <a href="http://www.saladworks.com/menu/locations">order online now. click here! </a>Voted the nation's #1 Salad Franchise by Entrepreneur Magazine for 2009 and 2010Voted one of the Top Ten Franchise Deals for 2010Saladworks, the nation's first and largest fresh-tossed salad franchise concept
    </marquee>
  </div>
  <div class="clearfix">&nbsp;</div>
  <div id="main">

<div class="lt"><a href="/fresh_fan_club2/Points">&lt;&lt; Back to Points Board</a></div>
<br>
<blockquote><blockquote>

<?php
	if($message)
	{
		echo "<br><h2 align='center'>$message</h2><br><br>";
	}
?>
<h2 align="center">did you visit a local saladworks and forget your fresh fan card? <br> no worries, simply enter in the transaction information from your receipt and we will add points to your card</h2>
</blockquote></blockquote>

<br>
<br>
<form action="receiptpoints.php" method="post">
<table cellpadding="3" cellspacing="0" border="0" align="center">
	<tr>
		<td>fresh fan club card</td>
		<td><input name="card" value="<?php echo $cardnum; ?>"></td>
	</tr>
	<tr>
		<td>store name</td>
		<td><input name="store" value="<?php echo $store; ?>"></td>
	</tr>
	<tr>
		<td>receipt number</td>
		<td><input name="receipt" value="<?php echo $receipt; ?>"></td>
	</tr>
	<tr>
		<td>receipt amount</td>
		<td>$<input name="amount" value="<?php echo $amount; ?>"></td>
	</tr>
	<tr>
		<td>date on receipt</td>
		<td><select name="receiptdate">
			<?php echo $receiptDateOptions; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="submit" value="Submit"></td>
	</tr>
</table>
</form>




<div class="clear"></div>
</div>
<div class="lt"><a href="javascript:;" onclick="modal_popup('terms_conditions','Terms And Conditions')">Terms &amp; Conditions</a></div>
<div class="lta"><a href="Account">Edit Information</a></div>
 </div>
    <div class="clear"></div>
  </div>
  <div id="footer">
    <div id="footer-links"> <a href="http://www.saladworks.com/gift_cards"><img alt="order customized gift cards for your friends and family" src="/images/gift_card_base.png"></a> <a href="http://www.saladworks.com/menu/locations"><img alt="Order Online" src="/images/online_base.png"></a> </div>

    <div id="foot_content"><a title="Home" href="http://www.saladworks.com/home">home</a> | <a title="Menu" href="http://www.saladworks.com/menu">menu</a> | <a title="Locations" href="http://www.saladworks.com/locations">locations</a> | <a title="Salad Franchise" href="http://www.saladworks.com/own_a_franchise">salad franchise</a> | <a title="About Us" href="http://www.saladworks.com/about_us">about us</a> | <a title="Contact Us" href="http://www.saladworks.com/contact_us">contact us</a> |  &nbsp; <br>

      &copy; 2010 Copyright Saladworks.com. All Rights Reserved.</div>
  </div>
</div>

</body>
</html>
