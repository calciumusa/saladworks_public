<?php
/*********
*  @filename:  birthdayUpdater.php
*   functions:
*               1. performs a query of the mySQL database "fan_club_users" where the birthday of the user matches today's date AND no birthday bonus was provided within the past 300 days, then takes that mySQL result set to generate an update query to the SQL database "Loyalty" adding a fixed number of bonus "birthday" points defined by $numPointsToAdd, set to expire 30 days later.
*               2. sends an automated email to an internal Saladworks representative, notifying execution summary; email address defined in $notifySaladworksEmail
*               3. sends an automated email to the loyalty club member
*   author: unknown, commenting/refactoring by Anson Han (Star Group 2013)
*
**********/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Birthday Updater script</title>
</head>
<body>

<?php
/***
* php execution debugging code
*/
  /*
  exit('Invalid');
  */
  ini_set('display_errors', 1);
  error_reporting(E_ALL);
  echo "<h1>birthdayUpdater.php debug statements enabled</h1>";
/*
* end php execution debugging code
***/



/***
* define global variables for execution
*/
  $numPointsToAdd = 800;  /* number of points to award for birthday month */
  //echo "Points set to: $numPointsToAdd";
  $notifySaladworksEmail = 'cmcelwee@saladworks.com';
/*
* end global variable declarations
***/



/***
* define date for mySQL where condition
*/
  /* automatic server date */
  $birthdate = date("m/d");  /* current server date (2-digits with leading zeroes) as month/day, ie 01/01 */
  $altbirthdate = date("n/j"); /* current server date (without leading zeroes) as month/day, ie 1/1 */

  /* manual date- uncomment section below to bypass server date; then define the date between the string delimiters */
  /*
  $birthdate = '01/01';
  $altbirthdate = date("1/1");
  */

  /* echo "<br/>Doing $birthdate<br/>"; */
/*
* end date declaration for mySQL where condition
***/



/* initialize first name field for birthday points email as null, in case none passed through from db query */
  $first = '';


include("includes/classes/db_settings.php");

/***
* declare mySQL connection
*/
  echo $mySQLdb . "@" . $mySQLhost . ", u: " . $mySQLuser . ", p: " . $mySQLpass . "<hr/>";
  $link = mysql_connect($mySQLhost, $mySQLuser, $mySQLpass);
  $db = mysql_select_db($mySQLdb, $link);
  unset ($mySQLdb, $mySQLhost, $mySQLuser, $mySQLpass);
/*
* end mySQL connection declaration
***/


/***
* declare SQL connection
*/
  echo $msSQLdb . "@" . $msSQLhost . ", u: " . $msSQLuser . ", p: " . $msSQLpass . "<hr/>";
  $mslink = mssql_connect($msSQLhost, $msSQLuser, $msSQLpass);
  mssql_select_db('Loyalty', $mslink);
  unset ($msSQLdb, $msSQLhost, $msSQLuser, $msSQLpass);
/*
* end SQL connection declaration
***/



/***
* mySQL query
*/
  /* define mySQL query */
  $sql = "
    SELECT u.`user_id`,u.`email`,u.`first_name`,u.`last_name`,u.`birthday`,c.`loyalty_card`
    FROM `fan_club_users` u, `fan_club_users_cards` c
    WHERE u.`user_id` = c.`user_id`
      AND ( u.`birthday` = '$birthdate' OR u.`birthday` = '$altbirthdate' )
      AND (
        u.lastBirthdayBonus < DATE_SUB(NOW(), INTERVAL 300 DAY)
        OR
        u.lastBirthdayBonus IS NULL
          )
    GROUP BY  u.`user_id`
  ";
  /* echo "<pre>$sql</pre><hr/>"; */

  /* perform the mySQL query */
  $result = mysql_query($sql,$link) or die(mysql_error());
/*
* end mySQL query
***/



/***
* loop through mysql records returned, construct and perform sql update, adding birthday points
*/
  /* initialize an array for the fields of each mySQL record returned */
  $usersToUpdate = array();

  while($row = mysql_fetch_assoc($result))
  {
    /* echo "<pre>";	print_r($row); echo "</pre>"; */
    $cardnum = str_replace('-','',$row['loyalty_card']); /* strip out any dashes from loyalty card number */
    $usersToUpdate[] = $row['user_id'];
    /* echo "Card: $cardnum <br/> With: " . $row['loyalty_card'] . " <br/><br/>"; */


    /* define msSQL query */
    $mssql = "
      update accounts set points = points + $numPointsToAdd
      , LastTransactionMerchantID = 335
      , LastTransactionDateTime = GETDATE()
      where accountnumber = '$cardnum'
    ";
    /*echo "<pre>$mssql</pre><br/>; */


    /* perform the msSQL query to loyalty card account, send notification of error updating to $notifySaladworksEmail contact */
    try
    {
      $r = mssql_query ( $mssql, $mslink );
    }
    catch (Exception $e)
    {
      mail($notifySaladworksEmail,"FFC Birthdays ERROR",print_r($e,true),"From: webmaster@saladworks.com\r\n");
      echo("FFC Birthdays ERROR" . print_r($e,true) . "From: webmaster@saladworks.com<br>lastmssqlmsg:" . mssql_get_last_message());
      exit();
    }

    /* define msSQL query */
    $mssql = "
      INSERT INTO [Loyalty].[dbo].[TransactionLog]
      ([AccountID]
      ,[MerchantID]
      ,[AccountNumber]
      ,[MerchantPOSID]
      ,[RefNo]
      ,[TranCode]
      ,[Points]
      ,[TransactionDateTime]
      ,[ReturnCode]
      ,[AddPoints]
      ,[TransactionLocalTimeOffset]
      ,[PointsBalance])
      VALUES
      ((SELECT AccountID FROM Accounts WHERE Accounts.AccountNumber = '$cardnum')
      ,335
      ,'$cardnum'
      ,'480000'
      ,'00'
      ,'Add'
      ,$numPointsToAdd
      ,getdate()
      ,'000000'
      ,$numPointsToAdd
      ,0
      ,(SELECT Points FROM Accounts WHERE Accounts.AccountNumber = '$cardnum'))
    ";
    /*echo "<pre>$mssql</pre><br/>; */


    /* perform the msSQL query to loyalty card transactions log*/
/*
    try
    {
      $r = mssql_query ( $mssql, $mslink );
    }
    catch (Exception $e)
    {
      mail($notifySaladworksEmail,"FFC Birthdays ERROR",print_r($e,true),"From: webmaster@saladworks.com\r\n");
      exit();
    }
*/

    /* perform the msSQL query to loyalty card transactions log*/
//    sendBirthdayEmail($row['first_name'],$row['last_name'],$row['email']);

  } /* end while statement */

  /* terminate mssql connection */
  mssql_close($mslink);

/*
* end loop through mysql records returned, perform sql update, adding birthday points
***/



/***
* conditional mysql update of matching records to reflect last time birthday bonus was awarded
*/
  $resultSummary = ''; /* initialize variable for mysql execution summary */
  if(count($usersToUpdate) > 0)
  {
    $sql = "UPDATE fan_club_users u SET u.lastBirthdayBonus = NOW() WHERE u.user_id IN (";
    /* generate array of user ids from original mySQL query results array*/
      $notFirstValue = false;
      foreach($usersToUpdate as $key => $value)
      {
        /* test to see if a delimiter is required for a list of values */
        if($notFirstValue) {
          $sql .= ',';
        }

        /* push user id to where condition IN array */
        $sql .= $value;
        $notFirstValue = true;
      }	/* end foreach */
    /* end generate array of user ids from original mySQL query results array*/
    $sql .= ")"; /* close sql statement where condition IN array */
    /* echo "<pre>$sql</pre><hr/>"; */


    /* perform the mySQL query */
    $result = mysql_query($sql,$link) or die(mysql_error());
    $resultSummary = "<br/><br/>Number Updated: " . count($usersToUpdate) . "<br/>";

    /* send notification of birthday point update summary to $notifySaladworksEmail contact */
    try
    {
      $m = mail($notifySaladworksEmail,count($usersToUpdate) . " Birthdays Updated for $birthdate",count($usersToUpdate) . " Birthdays Updated for $birthdate","From: "  . $notifySaladworksEmail . "\r\n");
      $resultSummary .= "<br/>mail() returned $m";
    }
    catch (Exception $e)
    {
      $resultSummary .=  'Caught exception: ',  $e->getMessage(), "\n";
    }
    /* end send notification of birthday point update summary to $notifySaladworksEmail contact */

  } /* end if statement in if-else, begin else */
  else
  {
    $resultSummary = "No one to update, quitting.";
  } /* end if-else */
  echo $resultSummary;
/*
* end conditional mysql update of matching records to reflect last time birthday bonus was awarded
***/


/* terminate mysql connection */
  mysql_close($link);



/***
* php execution debugging code
*/
  /*
  echo '<hr/>';
  echo preg_replace('/\n/', '<br/>', $msgbody);
  */
/*
* end php execution debugging code
***/


/***************************************************************************/

/***
*  @function: sendBirthdayEmail()
*   description: generate an email to users who've been awarded "birthday" points
*/

function sendBirthdayEmail($first,$last,$email)
{
  global $numPointsToAdd, $msgbody;

  $msgbody = "Happy Birthday $first!

  From all of us at Saladworks, we wish you a happy birthday!  Because you are a valued member of our Fresh Fan Club, we have added 800 birthday points to your account! Be sure to treat yourself soon because birthday points only stick around for 30 days.

  We hope you have a great birthday and you enjoy your visit to your favorite Saladworks location.  To check your Fresh Fan Club account balance at any time, login at http://www.saladworks.com/fresh_fan_club2/.

  Fanatic'ly,

  The Saladworks Team
  ";

  $headers = 'From: Saladworks <FreshFanClub@Saladworks.com>'."\r\n";
  mail($email,'Happy Birthday from Saladworks!',$msgbody,$headers);
}
/*
* end of sendBirthdayEmail() function
***/


?>
</body>
</html>
