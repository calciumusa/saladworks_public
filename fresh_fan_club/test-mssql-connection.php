<?php
/*********
*  @filename:  test-mssql-connection.php
*   function:  performs a select query of the SQL database "Loyalty"
*
**********/

echo "Server: $_SERVER[SERVER_ADDR]<br/>";

/// SHOW ALL ERRORS
ini_set('display_errors', 1);
error_reporting(E_ALL);

if(extension_loaded("mssql")) {
  echo "SQL Server Extension Loaded<br/>";
}
else {
  echo "SQL Server Extension NOT Loaded<br/>";
}
echo "Conn TEST<br/>";
flush();


include("includes/classes/db_settings.php");
define('MSSQLDB',$msSQLdb);
define('MSSQLHOST',$msSQLhost); //harvest
define('MSSQLUSER',$msSQLuser);
define('MSSQLPASS',$msSQLpass);
echo $msSQLdb . "@" . $msSQLhost . ", u: " . $msSQLuser . ", p: " . $msSQLpass . "<hr/>";

try {
	$link = mssql_connect(MSSQLHOST,MSSQLUSER,MSSQLPASS)  or die ( 'Can not connect to server' );
} catch(Exception $e) {
	echo 'Caught exception: ',  $e->getMessage(), "<br>";
}
unset ($msSQLdb, $msSQLhost, $msSQLuser, $msSQLpass);

echo "conn - $link<br>";
flush();

mssql_select_db('Loyalty', $link) or die ('Can not select DB');



//query
$sql = "select top 5 * from accounts";
$r = mssql_query ( $sql, $link ) or die ( 'Query Error' );


// loop the result
while ( $row = mssql_fetch_array ( $r ) )
{
	echo "$row[0]<br>";
}
mssql_close($link);


$hosts = array(/* array of hosts list */)

?>