/***********
*
*  @filename: loyalty.js
*  description:  custom js/php mvc controller for Fresh Fan Club site, works in conjunction with /includes/classes/users.class.php
*
*
************/

var $ = jQuery.noConflict();


/*----------------[DEFAULT JAVASCRIPT]----------------*/
var Domain;
var loc = new String(window.parent.document.location);

if (loc.indexOf("https://")!= -1) {
	var Domain = "https://"+document.domain+"/fresh_fan_club2/includes/ajax.inc.php";
} else {
	var Domain = "http://"+document.domain+"/fresh_fan_club2/includes/ajax.inc.php";
}

function post_request(parameters, returnfunction) {
	http_request = false;
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
	 http_request = new XMLHttpRequest();
	 if (http_request.overrideMimeType) {
		// set type accordingly to anticipated content type
		//http_request.overrideMimeType('text/xml');
		http_request.overrideMimeType('text/html');
	 }
	} else if (window.ActiveXObject) { // IE
	 try {
		http_request = new ActiveXObject("Msxml2.XMLHTTP");
	 } catch (e) {
		try {
		   http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
	 }
	}
	if (!http_request) {
	 alert('Cannot create XMLHTTP instance');
	 return false;
	}
	http_request.onreadystatechange = returnfunction;
	http_request.open('POST', Domain, true);
	http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_request.setRequestHeader("Content-length", parameters.length);
	http_request.setRequestHeader("Connection", "close");
	http_request.send(parameters);
}

function externalLinks() {
  if (!document.getElementsByTagName) return;
  var anchors = document.getElementsByTagName("a");
  for (var i=0; i<anchors.length; i++) {
    var anchor = anchors[i];
    if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external") {
		anchor.target = "_blank";
    }
    if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "protected") {
		anchor.target = "_blank";
    }
 }
}

addEvent(window,'load',externalLinks);

function addEvent( obj, type, fn ) {
  if ( obj.attachEvent ) {
    obj['e'+type+fn] = fn;
    obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
    obj.attachEvent( 'on'+type, obj[type+fn] );
  } else
    obj.addEventListener( type, fn, false );
}

function numbersonly(e){
	var unicode = e.charCode ? e.charCode : e.keyCode;
	if(unicode == 46 || unicode == 9) {
		return true;
	} else {
		if(unicode != 8){
			if(unicode < 48 || unicode > 57){
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
}

/*----------------[FRONT END JAVASCRIPT]----------------*/
function fan_club_register() {
	var frm=document.getElementById('fanclubregister');
	var list=new Array() ;
	for (i=0; i<frm.elements.length; i++){
		if(!frm.elements[i].value) {
			if(frm.elements[i].name != 'cell_phone' && frm.elements[i].name !='birthday') {
				alert('Please enter your information for '+	frm.elements[i].name.replace('_',' '));
				frm.elements[i].focus()
				return false;
			}
		}
		list[i] = frm.elements[i].name+"="+escape(frm.elements[i].value);
	}
	if(document.getElementById('password').value != document.getElementById('confirm_password').value) {
			alert('Confirmation Password does not match');
			return false;
	}
	var result2="ajax=fan_club_register&"+list.join("&")
	post_request(result2,function(){
		if(http_request.readyState == 4) {
			var data=http_request.responseText.split("^^");
			if(data[0]=='false') {
				alert(data[1]);
			} else {
				//document.getElementById('homeMain').innerHTML=data[0];
				//points_rollout(data[1]);
				window.location="/fresh_fan_club2/Points"
			}
			http_request.onreadystatechange = null;
		}
     });
	return false;
}

function link_login(frm) {
	fan_club_login(document.getElementById(frm));
}

function fan_club_login(frm) {
	var nmb=0;
	var list=new Array() ;
	for (i=0; i<frm.elements.length; i++){
		if(!frm.elements[i].value) {
			alert('All the Fields must be entered to Login!');
			frm.elements[i].style.background.color='#ccc';
			frm.elements[i].focus();
			return false;
		} else {
			list[i] = frm.elements[i].name+"="+escape(frm.elements[i].value);
		}
	}
	var result2="ajax=fan_club_login&"+list.join("&")
	post_request(result2,function(){
		if(http_request.readyState == 4) {
			var data=http_request.responseText.split("^^");
			if(data[0]=='false') {
				alert(data[1]);
			} else {
				//document.getElementById('homeMain').innerHTML=data[0];
				//points_rollout(data[1]);
				window.location="/fresh_fan_club2/Points"
			}
			http_request.onreadystatechange = null;
		}
	});
	return false;
}

function add_fan_club_card(frm) {
	if(document.getElementById('card').value) {
		var result2="ajax=add_loyalty_card&card_number="+document.getElementById('card').value;
		post_request(result2,function(){
			if(http_request.readyState == 4) {
				var data=http_request.responseText.split("^^");
				if(data[0]=='false') {
					alert(data[1]);
					document.getElementById('card').value='';
				} else {
					window.location="/fresh_fan_club2/Account"
					//points_rollout(data[1]);
				}
				http_request.onreadystatechange = null;
			}
		});
	} else {
		alert('Please enter a Card Number');
	}
	return false;
}

function points_rollout(points) {
	var results=points.split('::');
	document.getElementById('level_msg').className='set_height_notice';
	document.getElementById('level_msg').innerHTML=results[0];
	if(results[1] == 1) {
		document.getElementById('level1').style.background="#009A3D";
		document.getElementById('level1').style.color="#fff";
	}
	if(results[2] == 1) {
		document.getElementById('level2').style.background="#009A3D";
		document.getElementById('level2').style.color="#fff";
	}
	if(results[3] == 1) {
		document.getElementById('level3').style.background="#009A3D";
		document.getElementById('level3').style.color="#fff";
	}
	$(function() {
		$( "#accordion" ).accordion({
			autoHeight: false,
			navigation: true,
			collapsible: true
		});
	});
	primary_form_controls();
}

function primary_form_controls() {

  $.validator.setDefaults({
    submitHandler: function() {
      var frm = document.getElementById('fanclubregister');
      var list = new Array() ;
      for (i=0; i<frm.elements.length; i++) {
        list[i] = frm.elements[i].name+"="+escape(frm.elements[i].value);
      }
      var result2="ajax=fan_club_register&"+list.join("&")
      post_request(result2,function(){
        if(http_request.readyState == 4) {
          var data=http_request.responseText.split("^^");
          if(data[0]=='false') {
            alert(data[1]);
          } else {
            document.getElementById('l').innerHTML=data[0];
            //points_rollout(data[1]);
          }
          http_request.onreadystatechange = null;
        }
      }); /* post_request */
     } /* submitHandler */
  }); /* end $.validator.setDefaults */


  $().ready(function() {

    /***
    *  Mask phone number and date input field formats
    */
    $("#cell_phone").mask("(999) 999-9999");
    $("#birthday").mask("99/99");
    /*
    * end Mask phone number
    ***/

    /***
    *  Validation functionality for Fan Club Registration Form
    */
    $("#fanclubregister").validate({
      rules: {
        first_name: "required",
        last_name: "required",
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 5
        },
        confirm_password: {
          required: true,
          minlength: 5,
          equalTo: "#password"
        },
        birthday: "required"
      },
      messages: {
        firstname: "Please enter your firstname",
        lastname: "Please enter your lastname",
        email: "Please enter a valid email address",
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long"
        },
        confirm_password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long",
          equalTo: "Please enter the same password as above"
        },
        birthday: "Please enter your Birthday"
      }
    });
    /*
    * end fanclub registration form validation
    ***/

  }); /* end $(document).ready */

} /* end primary_form_controls */



/***
*    @function: modal_popup()
*    @paramenters: ajax_call (switch value for ajax.inc.php) and window title
*    description: generates popup window content for "forgot password" and "terms and conditions"
*
*/
function modal_popup(ajax_call,titlename) {
  var viewportHeight = $(window).height();
  var viewportWidth = $(window).width();
	var dialogOptions = {
		modal: false,
		bgiframe: true,
		autoOpen: false,
		height: viewportHeight * 0.75,
		position: 'top',
		width: viewportWidth * 0.85,
		draggable: true,
		resizeable: true,
		title: titlename,
		buttons: {
			"Close": function() {
				$(this).dialog("close");
			}
		}
	};
	$("#policy_dialog").dialog(dialogOptions);
	$("#policy_dialog").load(Domain+'?ajax='+ajax_call, [], function(){
	$("#policy_dialog").dialog("open");
	;}
	);
	return false;
}
/*
*   end @function modal_popup()
***/




/***
*    @function: send_reminder()
*    generates reminder email for users who've forgotten credentials to login
*/
function send_reminder(frm) {
	if(!frm.elements['email'].value) {
		alert('Please enter your email');
	}
	else {
		var result2="ajax=forgot_logon&email="+frm.elements['email'].value
		post_request(result2,function(){
			if(http_request.readyState == 4) {
				var data=http_request.responseText.split("^^");
				if(data[0]=='false' || !data[1]) {
					alert(data[1]);
				}
				else {
					document.getElementById('notice').innerHTML=data[1];
				}
				http_request.onreadystatechange = null;
			}
		 });
	}
	return false;
}
/*
*   end @function send_reminder()
***/