<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
/-------------------------------------------------------*/
#set a sudo secure Session
session_name('SIMMLLC_Session');
session_start();
ini_set("memory_limit", "-1");
ini_set("max_execution_time", 60 * 60 * 24);	// 1 DAY

#error settings should be off when live
error_reporting(0);
ini_set('display_errors',0);

#Set Default Time Zone to EST
date_default_timezone_set('America/New_York');

#Clear Cache to ensure all data is recent
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

#secure Page.  This should NOT be allowed to be viewed anywhere
if(!empty($_SERVER['REQUEST_URI'])) {
	$requestedURL = parse_url($_SERVER['REQUEST_URI']);
	if(basename($requestedURL['path']) === basename(__FILE__)) {
		header("location: /page-not-found");
	}
}
#Find absolute path to site
if(!empty($_SERVER['DOCUMENT_ROOT'])) {
	#Browser
	define('SITEPATH',$_SERVER['DOCUMENT_ROOT']."/");
} else {
	#Command Line Functionality
	define('SITEPATH','/var/www/vhosts/saladworks.com/httpdocs/');
}

define('WEBSITE',(!empty($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:'www.saladworks.com'));
define('URL',"/fresh_fan_club2");
define('LOGS',SITEPATH."fresh_fan_club2/logs/");
define('TEMPLATES',SITEPATH."fresh_fan_club2/includes/templates/");
define("SECONDS_PER_HOUR", 60*60);
define('HTTP_SITE','http://'.WEBSITE.URL.'/');
define('DEFAULT_MERCHANT','488888');
#Rosetta Security key
#!!!!!DO NOT CHANGE. CHANGING WILL RENDER PASSWORDS UNRECOVERABLE
define('SECURITY_PHRASE','Eat Harvest Fresh Salads');
define('SECURITY_KEY',' You will turn green');
define('SECURITY_LTRS','ABCDEFGH');
define('SECURITY_STYLE','57569278');

#Levels
define('LOW_PRODUCT','Cookie');
define('MEDIUM_PRODUCT','Cup Of Soup');
define('HIGH_PRODUCT','Any Salad');
define('LOW',500);
define('MEDIUM',1000);
define('HIGH',2000);
define('REGISTERPOINTS',200);


#PHP MAILER information
define('SITEEMAIL','online@saladworks.com');
define('SHORTNAME','Saladworks Website');
define('EMAILHOST','66.187.149.117');
define('HELO','saladworks.com');
define('EMAILLOGIN','franinfo@saladworks.com');
define('EMAILPASS','tivoli6');
define('SMTPAUTH',true); //Must add EMAILLOGIN and EMAILPASS values to login
define('BCC','bob@simmllc.com');
define('BCCNAME','Bob Babb');
#PHP Mailer Class
include_once("classes/phpmailer/class.phpmailer.php");
$mail=new PHPMailer;




include("classes/db_settings.php");

#MySQL connection settings
define('MYSQLDB',$mySQLdb);
define('MYSQLHOST',$mySQLhost);
define('MYSQLUSER',$mySQLuser);
define('MYSQLPASS',$mySQLpass);
#MYSQL Connection Class
include_once("classes/mysql.class.php");
$db=new SIMMLLC;

#MSSQL connection settings
define('MSSQLDB',$msSQLdb);
define('MSSQLHOST',$msSQLhost);//harvest
define('MSSQLUSER',$msSQLuser);
define('MSSQLPASS',$msSQLpass);
#MSSQL Connection Class
include_once("classes/mssql.class.php");
$mssql=new MSSQL;


#FTP Information
define('FTPSITE',$ftpSite);
define('FTP_USER',$ftpUser);
define('FTP_PASS',$ftpPass);


#image Magick Class
include_once("classes/imageMagick.class.php");
$magic=new ImageMagick;

#SHARED Functions Class
include_once("classes/shared.class.php");
$share=new SHARE($mail,$magic,$db,$mssql);

#COMTREX Class
include_once("classes/comtrex.class.php");
$comtrex=new COMTREX($db,$mssql,$share);

#MICROS Class
include_once("classes/micros.class.php");
$micros=new MICROS($db,$mssql,$share);

#USER Class
include_once("classes/users.class.php");
$user=new USERS($db,$mssql,$comtrex,$micros,$share);
?>