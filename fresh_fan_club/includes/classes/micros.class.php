<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
Micros Integration
/-------------------------------------------------------*/
if(!empty($_SERVER['REQUEST_URI'])) {
	$requestedURL = parse_url($_SERVER['REQUEST_URI']);
	if(basename($requestedURL['path']) === basename(__FILE__)) {
		header("location: /page-not-found");
	}
}

class MICROS {

    function __construct(&$db,&$mssql,&$share) {
		$this->db=$db;
		$this->db->debug=false;
		$this->mssql=$mssql;
		$this->mssql->debug=false;
		$this->share=$share;
		$this->logs="micros_log.log";
    }

	private function validate_store($store) {
		$this->db->query("select store_id from store_locations where store_no=".(int)$store);
		if($this->db->num_rows() == 0) {
			return NULL;
		} else {
			return $store;
		}
	}

	public function check_sync_time() {
		$store=$this->validate_store($_REQUEST['store_no']);
		if(!empty($store)) {
			$this->db->query("select * from micros_sync where store_id=".(int)$store);
			if($this->db->num_rows() == 0) {
				$return='NEW';
			} else {
				$this->db->movenext();
				if($this->db->col['last_sync'] == '0000-00-00 00:00:00' || $this->db->col['run_new']==2) {
					$return='NEW';
				} else {
					$return=$this->db->col['last_sync'];
				}
			}
			$this->share->append_file($this->logs,date("m/d/Y g:i A")." Received Request From Store: ".$_REQUEST['store_no']."\n");
		} else {
			$return='error';
		}
		return $return;
	}

	public function submit_orders() {
		$return=NULL;
		$num=array();
		$ignore=array('order_id','store_id','last_modified');
		$store=$this->validate_store($_REQUEST['store_no']);
		if(!empty($store)) {
			$flds=$this->db->show_columns('store_location_orders');
			foreach($flds as $f) {
				if(!in_array($f,$ignore)) {
					$fields[]=$f;
				}
			}
			$postText = trim(file_get_contents('php://input'));
			$this->share->record_log('fresh_fan_club2/test.xml',$postText);
			$data=$this->parse_xml($postText);
			if(!empty($data['item'])) {
				foreach($data['item'] as $r) {
					if(!empty($r->store_order_id)) {
						$num[]=$r->store_order_id;
						$insert=NULL;
						$sql ="insert into store_location_orders (store_id,".implode(',',$fields).",last_modified) ";
						$sql.="values ";
						foreach($fields as $c) {
							$insert[]="'".mysql_real_escape_string($r->$c)."'";
						}
						$sql.="($store,".implode(',',$insert).",NOW()) ";
						$sql.="ON DUPLICATE KEY UPDATE last_modified=NOW() ";
						$oid=$this->db->insert($sql);
						foreach($r->products->p_item as $k) {
							$sql ="insert into store_location_orders_items (order_id,line_no,item_cost,sale_item,last_modified,exported) ";
							$sql.=" values ";
							$sql.="($oid,".$k->line_no.",'".$k->item_cost."','".mysql_real_escape_string($k->sale_item)."',NOW(),'n') ";
							$sql.="ON DUPLICATE KEY UPDATE last_modified=NOW()";
							$this->db->update($sql);
						}
					}
				}
				$this->db->query("select order_date from store_location_orders where store_id=$store order by order_date desc limit 1");
				$this->db->movenext();
				$last_date=$this->db->col['order_date'];
				$this->db->update("insert into micros_sync values ($store,'$last_date',1,NOW()) ON DUPLICATE KEY UPDATE last_sync='$last_date',run_new=1,last_run=NOW()");
			}
			$n=array_unique($num);
			$this->share->append_file($this->logs,date("m/d/Y g:i A")." Processed (".count($n).") Order Requests From Store: $store\n");
		} else {
			$return='error';
		}
		return $return;
	}

	private function parse_xml($xml) {
		#Take XML Data and convert to array for easier handling
	    $root = simplexml_load_string($xml);
	    $data = get_object_vars($root);
		return $data;
	}

	public function export_orders_saladworks() {
		$this->db->query("select * from store_location_orders where exported='n'");
		$num=$this->db->num_rows();
		if($num != 0) {
			$this->share->append_file($this->logs,date("m/d/Y g:i A")." Export All Orders to Saladworks Harvest Server\n");
			$results=$this->db->get_records();
			foreach($results as $r) {
				$this->share->append_file($this->logs,date("m/d/Y g:i A")." Adding Order: ".$r['order_id']."\n");
				$insert=NULL;
				$sql ="use Micros; ";
				$sql.="insert into store_orders ";
				$sql.="(store_id,store_order_id,order_date,order_type,sale_total,taxes,";
				$sql.="cc_issue_number,cc_auth_code,cc_avail_balance,canceled_order,eAcctNum,last_modified";
				$sql.=") values (";
				$sql.=$r['store_id'].",".$r['store_order_id'].",'".$r['order_date']."','".$r['order_type']."','".$r['sale_total']."','".$r['taxes']."',";
				$sql.="'".$r['cc_issue_number']."','".$r['cc_auth_code']."','".$r['cc_avail_balance']."','".$r['canceled_order']."','".$r['eAcctNum']."','".$r['last_modified']."') ";
				$this->mssql->query($sql);
				$id=$this->mssql->get_last_insert_id('store_orders');
				if(!empty($id)) {
					$this->share->append_file($this->logs,date("m/d/Y g:i A")." Adding Order: ".$r['order_id']." Items\n");
					$this->db->query("Select * from store_location_orders_items where order_id=".(int)$r['order_id']);
					$yr=$this->db->get_records();
					foreach($yr as $y) {
						$value="($id,".$y['line_no'].",'".$y['item_cost']."','".$this->db->clean($y['sale_item'])."','".$r['last_modified']."')";
						$this->mssql->query("use Micros; insert into store_orders_items (order_id,line_no,item_cost,sale_item,last_modified) values $value");

					}
				}
				$this->db->update("update store_location_orders set exported='y' where order_id=".(int)$r['order_id']);
			}
			$this->share->append_file($this->logs,date("m/d/Y g:i A")." Completed Export (".$num.") Orders to Saladworks Harvest Server\n");
		}
	}

}
?>