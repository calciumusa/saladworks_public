<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
Comtrex Integration
/-------------------------------------------------------*/
if(!empty($_SERVER['REQUEST_URI'])) {
	$requestedURL = parse_url($_SERVER['REQUEST_URI']);
	if(basename($requestedURL['path']) === basename(__FILE__)) {
		header("location: /page-not-found");
	}
}

class COMTREX {
	
    function __construct(&$db,&$mssql,&$share) {
		$this->db=$db;
		$this->db->debug=false;
		$this->mssql=$mssql;
		$this->mssql->debug=false;
		$this->share=$share;
		$this->logs="comtrex_log.log";
		//$this->url='http://loyalty.comtrex.com:5646/LRequest.cgi';
		$this->url='http://harvest.saladworks.com:5646/LRequest.cgi';//66.187.149.119
		$this->loginbrandid=2;
		$this->login='BobBabb';
		$this->password='12345';//88babb88
    }
	
	public function get_card_activity($cards) {
		$sql ="USE Saladworks_ER;";
		$sql.="SELECT DailyLoyaltyTotals.Date,DailyLoyaltyTotals.StoreNumber,StoreDefinition.StoreName, ";
		$sql.="DailyLoyaltyTotals.LoyaltyAccountNumber,DailyLoyaltyTotals.ProductNumber,ProductDefinitions.ProductName ";
		$sql.="FROM DailyLoyaltyTotals ";
		$sql.="INNER JOIN ProductDefinitions ON DailyLoyaltyTotals.ProductNumber = ProductDefinitions.ProductNumber ";
		$sql.="INNER JOIN StoreDefinition ON DailyLoyaltyTotals.StoreNumber = StoreDefinition.StoreNumber ";
		$sql.="where (DailyLoyaltyTotals.LoyaltyAccountNumber IN ('".implode("','",$cards)."'));";
		$this->mssql->query($sql);
		if($this->mssql->num_rows() != 0) {
			$res=$this->mssql->get_records();
			foreach($res as $r) {
				$sql ="insert into loyalty_card_activity (loyalty_card,store_number,store_name,product_number,product,order_date,create_date,last_modified) values ";
				$sql.="('".$r['LoyaltyAccountNumber']."','".$r['StoreNumber']."','".$r['StoreName']."','".$r['ProductNumber']."','".$r['ProductName']."','".$r['Date']."',NOW(),NOW()) ";
				$sql.="ON DUPLICATE KEY UPDATE last_modified=NOW()";
				$this->db->update($sql);
			}
		}
		return true;
	}
	
	public function sync_card_groups() {
		#Syncronize Comtrex Card Numbers located on HARVEST SERVER to ensure the latest list is available.
		$this->db->query("SELECT [CardGroupID],[FirstCardNumber],[LastCardNumber] FROM [Loyalty].[dbo].[CardGroups]");
		$result=$this->db->get_records();
		foreach($result as $k) {
			$sql ="insert into loyalty_card_groups (card_group_id,card_start,card_end) values (".$k['CardGroupID'].",".$k['FirstCardNumber'].",".$k['LastCardNumber'].") ";
			$sql.="ON DUPLICATE KEY UPDATE card_start=".$k['FirstCardNumber'].",card_end=".$k['LastCardNumber'].",card_group_id=".$k['CardGroupID'];
			$this->db->update($sql);
			echo $k['FirstCardNumber']."<BR>";
		}
	}
	
	public function contact_comtrex($details) {
		#XML Default call to comtrex to retrieve / add / remove points and Balances
		$return=NULL;
		$array=array('TStream'=>array('Transaction'=>array('MerchantID'=>$details['merchant_id'],'OperatorID'=>0,'TranType'=>'Loyalty','Format'=>1,
					 'TranCode'=>$details['code'],'InvoiceNo'=>$details['invoice'],'RefNo'=>$details['ref'],
					 'Account'=>array('AcctNo'=>str_replace("-","",$details['account_number'])),
					 'Amount'=>array('Purchase'=>$details['purchase']),
					 'Items'=>array('Points'=>$details['points'],'Units'=>$details['units'],'Price'=>$details['price'],'PromoID'=>$details['promo_id']))));
		#CONVERT ARRAY TO XML
		$xml=$this->assemble_xml($array);
		#SUBMIT XML File
		//$this->share->append_file($this->logs,$xml); 
		$results=$this->call_comtrex($xml);
		#CONVERT RETURNED XML TO ARRAY
		$return=$this->parse_xml($results);
		return $return;
	}
	
	private function parse_xml($xml) {
		#Take XML Data and convert to array for easier handling
	    $root = simplexml_load_string($xml);
	    $data = get_object_vars($root);
		return $data;
	}
	
	private function assemble_xml($data) {
		/*
		# Private Function Build XML string based on $data Array
		# $xml = XML Objects
		*/
		$xml=NULL;
		foreach($data as $key => $value) {
			if(is_array($value)) {
				$xml.='<'.$key.'>'.$this->assemble_xml($value).'</'.$key.'>';
			} else {
				$xml.='<'.$key.(!empty($value)?'>'.utf8_encode($value).'</'.$key.'>':' />');
			}
		}
		return $xml;
	}
	
	private function call_comtrex($xml) {
		#Primary Call to Comtrex Web Service.  Apply URL, LOGIN, XML Data
		$header[]="loginbrandid:".$this->loginbrandid;
		$header[]="loginname:".$this->login;
		$header[]="loginpassword:".$this->password;
		$header[]="Content-type:text/xml";
		$header[]="Content-length:".strlen($xml);

		$ch = curl_init($this->url);
		curl_setopt($ch,CURLOPT_HEADER,0);
		curl_setopt($ch,CURLOPT_POST,1);  
		curl_setopt($ch,CURLOPT_TIMEOUT,60);  
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
		curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$xml);
		$result=curl_exec($ch);
		if(curl_errno($ch)) {
			##Record Error for tracing.
			$error="[".date("m/d/Y g:i A")."] CURL ERROR --> ".curl_errno($ch).": ".curl_error($ch);
			$this->share->append_file($this->logs,$error);
			$result=false;
		}
		curl_close($ch);
		return $result;
	}
}
?>