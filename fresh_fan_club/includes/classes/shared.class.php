<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
Shared Class..  Functions that can be used on all services
/-------------------------------------------------------*/
if(!empty($_SERVER['REQUEST_URI'])) {
	$requestedURL = parse_url($_SERVER['REQUEST_URI']);
	if(basename($requestedURL['path']) === basename(__FILE__)) {
		header("location: /page-not-found");
	}
}

class SHARE {
	
	function __construct(&$mail,&$magic,&$db,&$mssql) {
		$this->phpmail=$mail;
		$this->magic=$magic;
		$this->db=$db;
		$this->mssql=$mssql;
	}
	
	public function page_defaults() {
		return array("title"=>NULL,"heading"=>NULL,"content"=>NULL,"subject"=>NULL,"specials"=>NULL,"top_java"=>NULL,"bottom_java"=>NULL,"ssl"=>NULL,"headertitle"=>NULL,"headertext"=>NULL);
	}
	
	public function bad() {
		return array('`','~','!','@','#','$','%','^','&','*','(',')','\ ','=','+','[',']','{','}',';',':','"',"'",'<',',','.','>','/','?','\\','|',' ');
	}
	
	public function clean_url($url) {
		return preg_replace('/[^A-Za-z0-9]/','-',$url);
	}
	
	public function limit_words($string)	{
		$words = explode(" ",$string);
		return implode(" ",array_splice($words,0,WORD_LIMIT));
	}
	
	public function stores($layout=NULL) {
		$return=NULL;
		$this->db->query("select * from store_locations order by name");
		$results=$this->db->get_records();
		switch($layout) {
			case "option":
				foreach($results as $r) {
					$name=explode("|",$r['name']);
					$return.="<option value=\"".$r['store_no']."\">".trim((!empty($name[0])?$name[0]:$name[1]))."</option>\n\t";	
				}
			break;
			default:
				$return=$results;
			break;
		}
		return $return;
	}
	
	public function name_format($type) {
		$array['prefix']=array('Mr.','Ms.','Miss','Mrs.','Master','Rev.','Fr.','Dr.','Atty.','Prof.','Hon.','Pres.','Gov.','Ofc.','Msgr.','Sr.','Br.',
							   'Supt.','Rep.','Sen.','Amb.','Treas.','Sec.','Pvt.','Cpl.','Sgt.','Adm.','Maj.','Capt.','Cmdr.','Lt.','Lt Col.','Col.','Gen.');
		$array['suffix']=array('I','II','III','Sr.','Jr.','Ph.D.','M.D.','B.A.','M.A.','D.D.S.');
		sort($array[$type]);
		return $array[$type];
	}
	
	public function view_page($page) {
		$read=NULL;
		$details=NULL;
		if(!is_file($page.'.html')) {
			header("HTTP/1.0 404 Not Found");
			$read=file_get_contents(TEMPLATES.'page-not-found.html');
		} else {
			$read=file_get_contents($page.'.html');
		}
		$data=array("TITLE","HEADING","CONTENT","SUBJECT","SPECIALS","TOP_JAVA","BOTTOM_JAVA","SSL","HEADERTITLE","HEADERTEXT");
		foreach($data as $d) {
			$match=NULL;
			$field=preg_match("/({1:".$d."})(.*)({2:".$d."})/msU",$read,$match);
			if(!empty($match)) {
				$details[strtolower($d)]=utf8_encode($match[2]);
			} else {
				$details[strtolower($d)]=NULL;
			}
		}
		return $details;
	}
	
	public function append_file($file,$write) {
		$f = fopen(LOGS.$file, "a+");
		fputs($f, $write, strlen($write));
		fclose($f);
		return true;
	}

	public function read_file($file) {
		return file_get_contents($file);
	}
	
	public function record_log($file,$data) {
		$f = fopen(SITEPATH.$file, "w+");
		fputs($f, $data, strlen($data));
		fclose($f);
		return true;
	}

	public function get_files($get) {
		$f = fopen(SITEPATH.$get['file'], $get['type']) or die('unknown');
		$read=fread($f,$get['length']);
		fclose($f);
		return $read;
	}
	
	public function ftp_tools($tool,$values) {
		$return=NULL;
		$conn_id = ftp_connect(FTPSITE);
		$login_result = ftp_login($conn_id, FTP_USER, FTP_PASS) or die('unable to connect');
		switch($tool) {
			case "chmod":
				if (ftp_chmod($conn_id, $values['grade'], $values['folder']) !== false) {
					$result=true;
				} else {
					$result=false;
				}
			break;
		}
		ftp_close($conn_id);
		return $return;
	}
	
	public function check_folder($folder,$ignore,$list_type) {
		$return=NULL;
		if ($handle = opendir($folder)) {
			while (false !== ($file = readdir($handle))) {
				$s_file=NULL;
				if ($file != "." && $file != "..") {
					if(!empty($list_type)) {
						$ex=explode('.',$file);
						if(end($ex) == $list_type) {
							$s_file=$file;
						}
					} else {
						$s_file=$file;	
					}
					if(!empty($s_file)) {
						if(!empty($ignore)) {
							if(!in_array($file,$ignore)) {
								$return[]=$s_file;
							}
						} else {
							$return[]=$s_file;
						}
					}
				}
			}
			closedir($handle);
		}
		return $return;
	}
	
	public function walk_dir($path) {
	    $retval=array();
	    if ($dir = opendir($path)) {
	      while (false !== ($file = readdir($dir))) {
		    if ($file[0]==".") continue;
		    if (is_dir($path."/".$file) && $file != 'Image')
		      $retval= array_merge($retval,$this->walk_dir($path."/".$file));
		    else if (is_file($path."/".$file))
		      $retval[]=array('path'=>$path,'file'=>$file);
		  }
	      closedir($dir);
	    }
	    return $retval;
	}

	private function has_no_headers() {
		if ( preg_match( "/url=|link=|bcc|http|Content-Type/i", implode( $_POST ) ) ){
			return 'Y';
		} else {
			return 'N';
		}
	}
	
	public function get_fan_club_user($user_id) {
		$this->db->query("select * from fan_club_users where user_id=".(int)$user_id);
		$results=$this->db->movenext();
		return $results;
	}
	
	public function available_user_points() {
		$points=0;
		$session_points=0;
		$fan_club_user=$this->get_fan_club_user($_SESSION['user_id']);
		$sql ="SELECT SUM(loyalty_temp_orders_item.quantity*loyalty_products.points) as total ";
		$sql.="FROM loyalty_temp_orders_item ";
		$sql.="Inner Join loyalty_temp_orders ON loyalty_temp_orders.temp_id = loyalty_temp_orders_item.temp_id ";
		$sql.="Inner Join loyalty_products ON loyalty_products.product_id = loyalty_temp_orders_item.product_id ";
		$sql.="where loyalty_temp_orders.user_session='".session_id()."' ";
		$this->db->query($sql);
		if($this->db->num_rows() != 0) {
			$r=$this->db->movenext();
			$points=$r['total'];
		}
		return array('remaining'=>($fan_club_user['points']-$points),'OrderPoints'=>$fan_club_user['points'],'Recorded'=>$points);
	}
	
	public function states() {
		$this->db->query("select * from states order by state");
		return $this->db->get_records();
	}
	
	public function send_email($message) {
		$OB="----=_OuterBoundary_000";
		$IB="----=_InnerBoundery_001";
		$headers ="MIME-Version: 1.0\n";
		$headers.="From: ".SHORTNAME." <".SITEEMAIL.">\n";
		$headers.="Reply-To: ".SHORTNAME." <".SITEEMAIL.">\n";
		$headers.="X-Priority: 3\n";
		$headers.="X-MSMail-Priority: Normal\n";
		$headers.="X-Originating-IP: ".$_SERVER['SERVER_ADDR']."\n";
		$headers.="X-Mailer: PHP Mailer\n";
		$headers.="Content-Type: multipart/mixed;\n\tboundary=\"".$OB."\"\n";
	
		$Msg ="This is a multi-part message in MIME format.\n";
		$Msg.="\n--".$OB."\n";
		$Msg.="Content-Type: multipart/alternative;\n\tboundary=\"".$IB."\"\n\n";
	
		$Msg.="\n--".$IB."\n";
		$Msg.="Content-Type: text/plain;\n\tcharset=\"iso-8859-1\"\n";
		$Msg.="Content-Transfer-Encoding: quoted-printable\n\n";
		$Msg.=str_replace("</li>","\n",$message['alt_form'])."\n\n";
	
		$Msg.="\n--".$IB."\n";
		$Msg.="Content-Type: text/html;\n\tcharset=\"iso-8859-1\"\n";
		$Msg.="Content-Transfer-Encoding: base64\n\n";
		$Msg.=chunk_split(base64_encode($message['form']))."\n\n";
	
		$Msg.="\n--".$IB."--\n";
	
		if(!empty($message['file']) && is_array($message['file'])){
			foreach($message['file'] as $AttmFile){
				$patharray = explode ("/", $AttmFile);
				$FileName=$patharray[count($patharray)-1];
				$Msg.= "\n--".$OB."\n";
				$Msg.="Content-Type: application/octetstream;\n\tname=\"".$FileName."\"\n";
				$Msg.="Content-Transfer-Encoding: base64\n";
				$Msg.="Content-Disposition: attachment;\n\tfilename=\"".$FileName."\"\n\n";
	
				$fd=fopen ($AttmFile, "r");
				$FileContent=fread($fd,filesize($AttmFile));
				fclose ($fd);
				$FileContent=chunk_split(base64_encode($FileContent));
				$Msg.=$FileContent;
				$Msg.="\n\n";
			}
		}
	
		$Msg.="\n--".$OB."--\n";
	
		(int)$ret_val = mail($this->mail_string($message['email'], false, true), $this->mail_string($message['subject']), $Msg, $headers, '-f'.SITEEMAIL);
		if($ret_value != 1) {
			$error="[".date("m-d-Y g:i A")."]-Unable to send to email: (".$ret_val.") Email as follows\r\n".$message['form']."\r\n";
			$this->append_file("email_log.log",$error);
		}
		return true;
	
	}
	
	function mail_string($mailString, $allow_html=false, $allow_repeats=true) {	
		$mailString = preg_replace("/(\r\n|\n|\r|\;)/", "", $mailString);
		if(!$allow_html){
			$mailString = strip_tags($mailString);
		}
		return $mailString;
	}

	/*
	public function send_email($msg){
		$this->phpmail->Mailer   = "smtp";
		$this->phpmail->From     = SITEEMAIL;
		$this->phpmail->FromName = SHORTNAME;
		$this->phpmail->Host     = EMAILHOST;
		$this->phpmail->Helo		= HELO;
		$this->phpmail->IsHTML   = true;
		$this->phpmail->Port		= 25;
		$this->phpmail->ContentType = 'text/html';
		$this->phpmail->SMTPAuth	= SMTPAUTH; 
		$this->phpmail->Username	= EMAILLOGIN; 
		$this->phpmail->Password	= EMAILPASS; 
		$this->phpmail->Subject	= $msg['subject'];
		$this->phpmail->Body		= $msg['form'];
	  	$this->phpmail->AltBody  = $msg['alt_form'];
		$this->phpmail->AddAddress($msg['email'], $msg['name']);
		$this->phpmail->AddBCC(BCC,BCCNAME);
		if(!empty($msg['file'])) {
			$this->phpmail->AddStringAttachment($msg['file'], $msg['file_name']);
		}
		if(!$this->phpmail->Send()) {
			$error="[".date("m-d-Y g:i A")."]-Unable to send to email: ".$msg['email']." Error Information: ".$this->phpmail->ErrorInfo."\r\n";
			$this->append_file("email_log.log",$error);
		}
		$this->phpmail->ClearAddresses();
		$this->phpmail->ClearAttachments();
	}
	*/
	
	// Encrypt data
	public function stone($data)     {
		$encrypted1 = mcrypt_cbc(MCRYPT_BLOWFISH, SECURITY_PHRASE, $data, MCRYPT_ENCRYPT, SECURITY_STYLE);
		$encrypted2 = mcrypt_cbc(MCRYPT_TRIPLEDES, SECURITY_KEY, $encrypted1, MCRYPT_ENCRYPT, SECURITY_LTRS);
		return base64_encode($encrypted2);
	}
	// Decrypt data
	public function sober($data){
		$encrypted1 = mcrypt_cbc(MCRYPT_TRIPLEDES, SECURITY_KEY, base64_decode($data), MCRYPT_DECRYPT, SECURITY_LTRS);
		$plaintext = mcrypt_cbc(MCRYPT_BLOWFISH, SECURITY_PHRASE, $encrypted1, MCRYPT_DECRYPT, SECURITY_STYLE);
		return rtrim($plaintext);
	}

	function image_magic($tool,$array,$folder) {
		switch($tool) {
			case "constrain":
				$this->magic->constrain($array['file'],$array['width'],$array['height'],$array['file_new']);
			break;
			case "convert":
				$this->magic->convert_to_jpg($array['path'],$array['file'],$array['file_new']);
			break;
			case "constrain_width":
				$this->magic->constrain_by_width($array['file'],$array['width'],$array['folder']);
			break;
			case "constrain_height":
				$this->magic->constrain_by_height($array['file'],$array['folder'],$array['height']);
			break;
		}
	}
	
	private function clean_tags($cnt) {
		$sitecontents=NULL;
		$sitecontents = trim($cnt);
		$sitecontents = chop($sitecontents);
		$clear=array("&nbsp;","&ndash","&rsquo;","&rdquo;","&ldquo;","&#8482","\t","\r\n","\r","\n","<br />","<br>","`","\"","-");
		$sitecontents = str_replace ($clear, " ", $sitecontents);
		$sitecontents = strip_tags($sitecontents);
		while(!(strpos($sitecontents,"  ",0)===false) ){
			$sitecontents = str_replace ("  ", " ", $sitecontents);
		}
		return $sitecontents;
	}

	public function keywords($sitec,$data=NULL) {
        $keys=array();
		$clean=array(",",".",";",":",")","(","\"","?","!","{","}","[","]");
        $keyword_result=NULL;
        $sitecontents = $this->clean_tags($sitec);
        $sitecontents = ucfirst(trim($sitecontents));

        $parsearray[] = $sitecontents;
        $parsestring = " ".strtolower(join($parsearray," "))." ";
        $parsestring = str_replace ($clean, "", $parsestring);
      //  $parsestring = preg_replace("|[^A-Za-z0-9]|"," ",$parsestring);//str_replace ($clean, "", $parsestring);
		#Enter words here that you do NOT want to show
        $commonwords ="the a & because should something early few since through during others around long before sure click go hour hours am pm any does if i you he she to here been still km well ";
        $commonwords.="look other him must whom within has also two often only than want without when of if can while was and it in that with my so at for up on by this from be as me some her she ";
        $commonwords.="time again were down back would his how just brother both all one needed not had after there out lot quite do may see many know always no except anyone | per them the very ";
        $commonwords.="they their most then about but like who your will we is are or our have an more what us which its being into later these following \\ such over ensure months shopping cart ";
        $commonwords.="items (0) home about us search: vehicle specific top sellers too t re ra s ma \- - check everything pc allows 1 2 3 4 5 6 7 8 9 0 w/ / you'll bm factory systems piece products ";
        $commonwords.="isn?t functionality enough notch let ordinary selection 198 &amp;amp";
        $commonarray = split(" ",$commonwords);

        for ($i=0; $i<count($commonarray); $i++) {
           $parsestring = str_replace (" ".$commonarray[$i]." ", " ", $parsestring);
        }

        $wordsarray = split(" ",$parsestring);

        for ($i=0; $i<count($wordsarray); $i++) {
           $wordsarray[$i] = trim(chop($wordsarray[$i]));
                if( $wordsarray[$i]<>"" ){
                        $word = $wordsarray[$i];
                        if (isset($freqarray[$word])) {
                                 $freqarray[$word] += 1;
                        } else {
                                 $freqarray[$word]=1;
                        }
                }
        }

        @arsort($freqarray);
        $expectwords ="saladworks Fun Fresh Fan Club Best Salad Salads Franchise";
        @reset($freqarray);

        $i=0;
        while (list($key, $val) = @each($freqarray)) {
           $i++;
           $freqall[$key] = $val;
           if ($i==15) {
                  break;
           }
        }
        $freqarray2='';
        for ($i=0; $i<count($wordsarray)-1; $i++) {
           $j = $i+1;
           $word2 = $wordsarray[$i]." ".$wordsarray[$j];
           if (isset($freqarray2[$word2])) {
                   $freqarray2[$word2] += 1;
           } else {
                   $freqarray2[$word2]=1;
           }
        }

        @arsort($freqarray2);

        $i=0;
        while (list($key, $val) = @each($freqarray2)) {
           $i++;
           $freqall[$key] = $val;
           if ($i==4) {
                  break;
           }
        }
        $freqarray3='';
        for ($i=0; $i<count($wordsarray)-2; $i++) {
           $j = $i+1;
           $word3 = $wordsarray[$i]." ".$wordsarray[$j]." ".$wordsarray[$j+1];
           if (isset($freqarray3[$word3])) {
                   $freqarray3[$word3] += 1;
           } else {
                   $freqarray3[$word3]=1;
           }
        }

        @arsort($freqarray3);

        $i=0;
        while (list($key, $val) = @each($freqarray3)) {
           $i++;
           $freqall[$key] = $val;
           if ($i==1) {
                  break;
           }
        }
        if(!empty($goldwords)) {
                for ($a=0; $a<count($goldwords); $a++) {
                        if(!empty($freqall[$goldwords[$a]])) {
                                $freqall[$goldwords[$a]] = $freqall[$goldwords[$a]] + 1;
                        }
                }
        }

        @arsort($freqall);
        $keys='';
        $listarray='';
        while (list($key, $val) = @each($freqall)) {
           //$listarray .= "$key = $val<br/>";
           if(trim(strlen($key)) > 2) {
                $keys[]=trim(str_replace("&","&amp;",$key));
           }
        }
        $keys=(!empty($keys)?array_unique($keys):array());
        $kys=implode(', ',$keys);
        $substr = strlen($kys);
        $keyword = stripslashes(substr($kys,0,$substr-1));
        $keyword = chop($keyword);
        $sitecontents=str_replace("&","&amp;",$sitecontents);
        $words = explode(' ', $sitecontents);
        $words = array_unique($words);
        $desc = implode(' ', array_slice($words, 0, 25));
        $return=array("keywords"=>$keyword,"desc"=>$desc);
        return $return;
	}
}
?>
