<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
Back End Web Service
/-------------------------------------------------------*/
$requestedURL = parse_url($_SERVER['REQUEST_URI']);
if(basename($requestedURL['path']) === basename(__FILE__)) {
	header("location: /Page-Not-Found");
}

class WEB_SERVICE {
	
	function __construct(&$db,&$mssql,&$comtrex,&$micros,&$share) {
		$this->db=$db;
		$this->db->debug=false;
		$this->mssql=$mssql;
		$this->mssql->debug=false;
		$this->micros=$micros;
		$this->comtrex=$comtrex;
		$this->share=$share;
	}
	
	public function switch_post() {
		switch($_REQUEST['q']) {
			case "check_time":
				$content=$this->micros->check_sync_time();
			break;
			case "orders":
				$content=$this->micros->submit_orders();
			break;
		}
		return $content;
	}
	
	function s_test() {
		$this->db->query("select * from loyalty_card_groups");
		var_dump($this->db->get_records());
	}
	
}
?>