<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (proprietor)
bob@simmllc.com
215-853-2623
/-------------------------------------------------------*/

class ImageMagick {
    
    function __construct() {
    	$this->convert = 'convert';
    }

    function constrain($file_path,$int_max_width,$int_max_height,$new_name=NULL) {
        if (file_exists($file_path)) {
            $int_imgsize = getimagesize($file_path);
            if ($int_imgsize[0] > $int_max_width || $int_imgsize[1] > $int_max_height) {
                exec($this->convert." -geometry ". $int_max_width ."x". $int_max_height ." ". $file_path ." ". (!empty($new_name)?$new_name:$file_path));
            } 
        } else {
            die($file_path ." does not exist");
        }
        return;
    }
	
    function constrain_by_width($file_path,$int_max_width,$new_name=NULL) {
        if (file_exists($file_path)) {
            $int_imgsize = getimagesize($file_path);
            if ($int_imgsize[0] > $int_max_width) {
                exec($this->convert ." -geometry ". $int_max_width ." ". $file_path ." ". (!empty($new_name)?$new_name:$file_path));
            } 
        } else {
            die($file_path ." does not exist");
        }
        return;
    }
	
	function convert_to_jpg($file,$filname,$filenewname) {
		exec($this->convert ." -trim -rotate 90 ".$file."/".$filname." ".$file."/".$filenewname);
		list($width, $height, $type, $attr) = getimagesize($file."\\".$filenewname);
		if($width > 637) {
			exec($this->convert ." -rotate 90 ".$file."/".$filenewname);
		}
		return;
	}
	
    function constrain_by_height($file_path,$int_max_height,$new_name=NULL) {
        if (file_exists($file_path)) {
            $int_imgsize = getimagesize($file_path);
            if ($int_imgsize[0] > $int_max_height) {
                exec($this->convert ." -geometry x". $int_max_height ." ". $file_path ." ". (!empty($new_name)?$new_name:$file_path));
            } 
        } else {
            die($file_path ." does not exist");
        }
        return;
    }
} 
?>