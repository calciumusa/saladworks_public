<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (proprietor)
bob@simmllc.com
215-853-2623
Microsoft SQL Connection
/-------------------------------------------------------*/
class MSSQL {
	
	function __construct() {
		$this->link_id=NULL;
		$this->result=NULL;
		$this->col=NULL;
		$this->query=NULL;
		$this->fields=NULL;
		$this->records=NULL;
		$this->error=NULL;
		$this->debug = false;
		$this->debug_file = "mssql_log.log";
		$this->query_count = 0;
		$this->link_id = mssql_connect(MSSQLHOST,MSSQLUSER,MSSQLPASS);
		@mssql_select_db(MSSQLDB,$this->link_id);
	}

	private function record_sql($file,$write) {
		$f = fopen(LOGS.$file, "a+");
		fputs($f, $write, strlen($write));
		fclose($f);
		return true;
	}

	function query($_query){
		#Query SQL
		list($usec, $sec) = explode(" ",microtime());
		$time_start  = ((float)$usec + (float)$sec);
		
		$this->query = $_query;
		$this->result = @mssql_query($_query, $this->link_id);
		
		list($usec, $sec) = explode(" ",microtime());
		$time_end  =  ((float)$usec + (float)$sec);
		$time = $time_end - $time_start;

		if($this->debug || $this->result==false){
			$this->query_count ++;
			$msg="# ".date("m-d-Y g:i A")." Error -> ".@mssql_get_last_message($this->link_id)." Query ->".$_query."\n";
			$this->record_sql($this->debug_file,$msg);
		}
	}
	
	function num_rows() {
		#INT Number of Records
		return (int)@mssql_num_rows($this->result);	
	}
	
	function get_records(){
		#Multiple Results
		$this->records = array();
		while($row = @mssql_fetch_assoc($this->result)){
			$this->records[count($this->records)]= $row;
		}
		reset($this->records);
		return $this->records;
	}
	
	function movenext(){
		#Single Result
		return $this->col=@mssql_fetch_assoc($this->result);
	}
	
	function free_result(){
		#Clear SQL Cache
		mssql_free_result($this->result);
	}
	
	function close_db(){
		#Close SQL Connection
		mssql_close($this->link_id);
	}
	
	function get_last_insert_id($table) {
		$this->query("select SCOPE_IDENTITY() AS last_insert_id from $table");
		$re=$this->movenext();
		return $re['last_insert_id'];
	}
}
?>