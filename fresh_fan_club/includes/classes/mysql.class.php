<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
/-------------------------------------------------------*/
if(!empty($_SERVER['REQUEST_URI'])) {
	$requestedURL = parse_url($_SERVER['REQUEST_URI']);
	if(basename($requestedURL['path']) === basename(__FILE__)) {
		header("location: /page-not-found");
	}
}

class SIMMLLC {
	
	var $link_id;
	var $result;
	var $col;
	var $query;
	var $fields;
	var $records;
	var $error;
	
	var $debug = false;
	var $debug_file = "mysql_log.log";
	var $query_count = 0;

	function __construct(){
		#Establish Connection
		$this->fields = array();
		$this->link_id = @mysql_connect(MYSQLHOST,MYSQLUSER,MYSQLPASS) or trigger_error(mysql_error(),E_USER_ERROR);
		@mysql_select_db(MYSQLDB, $this->link_id);
	}
		
	function reset(){
		#Field Setup
		$this->fields = array();
	}
	
	function clean($value) {
		return mysql_real_escape_string($value);	
	}
	
	function insert($sql){
		#Insert Record return ID
		$this->unbuffered_query($sql);
		return $this->insert_id();
	}
	
	function update($sql){
		#Update Query
		$this->unbuffered_query($sql);
	}
	
	private function record_sql($file,$write) {
		$f = fopen(LOGS.$file, "a+");
		fputs($f, $write, strlen($write));
		fclose($f);
		return true;
	}

	function query($_query){
		#SQL Query
		list($usec, $sec) = explode(" ",microtime());
		$time_start  = ((float)$usec + (float)$sec);
		
		$this->query = $_query;
		$this->result = @mysql_query($_query, $this->link_id);
		
		list($usec, $sec) = explode(" ",microtime());
		$time_end  =  ((float)$usec + (float)$sec);
		$time = $time_end - $time_start;
		
		if($this->debug || $this->result==false){
			$this->query_count ++;
			$msg="# ".date("m-d-Y g:i A")." Error -> ".mysql_error()." Query ->".$_query."\n";
			$this->record_sql($this->debug_file,$msg);
		}
	}
	
	function unbuffered_query($_query){
		#Unbuffered query to save memory usage
		list($usec, $sec) = explode(" ",microtime());
		$time_start  = ((float)$usec + (float)$sec);
		
		$this->query = $_query;
		$this->result = @mysql_unbuffered_query($_query, $this->link_id);
		
		list($usec, $sec) = explode(" ",microtime());
		$time_end  =  ((float)$usec + (float)$sec);
		$time = $time_end - $time_start;

		if($this->debug || $this->result==false){
			$this->query_count ++;
			$msg="# ".date("m-d-Y g:i A")." Error -> ".mysql_error()." Query ->".$_query."\n";
			$this->record_sql($this->debug_file,$msg);
		}
	}
	
	function show_columns($table) {
		#Table Columns
		$this->columns = array();
		$this->query("show columns from ".$table);
		return $this->get_records('Field');
	}
	
	function get_records($required_field=NULL){
		#Return Array Of Multiple Values
		$this->records = array();
		while($row = @mysql_fetch_array($this->result, MYSQL_ASSOC)){
			$this->records[count($this->records)] = (empty($required_field)?$row:$row[$required_field]);
		}
		reset($this->records);
		return $this->records;
	}
	
	function get_tables_status(){
		#Get SQL Tables Status
		$this->query("SHOW TABLE STATUS FROM ".$this->db_name);
		if($this->num_rows() > 0){
			$tables = array();
			while($this->movenext()){
				$tables[$this->col["Name"]] = $this->col;
			}
			return $tables;
		}
		return false;
	}
	
	function fetch_array(){
		#Fetch Field Row Array
		$this->col = @mysql_fetch_array($this->result, MYSQL_BOTH);
	}
	
	function num_rows(){
		#INT Number of Records
		return (int)@mysql_num_rows($this->result);
	}
	
	function movenext(){
		#Retrun Single Row
		return $this->col=@mysql_fetch_array($this->result, MYSQL_ASSOC);
	}
	
	function insert_id(){
		#Last Inserted ID
		return @mysql_insert_id($this->link_id);
	}
	
	function error(){
		#SQL Error
		return @mysql_error($this->result, MYSQL_BOTH);
	}
	
	function mysql_flush() {
		#Flush SQL Cache
		return @mysql_free_result($this->link_id);
	}
	
	function close_db(){
		#Close SQL Connection
		@mysql_close($this->link_id);
	}
}
?>