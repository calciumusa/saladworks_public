<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
FRONT END WEBSITE
/-------------------------------------------------------*/
$requestedURL = parse_url($_SERVER['REQUEST_URI']);
if(basename($requestedURL['path']) === basename(__FILE__)) {
	header("location: /Page-Not-Found");
}

class SITE {

	function __construct(&$db,&$mssql,&$comtrex,&$micros,&$share,&$user) {
		$this->db=$db;
		$this->db->debug=false;
		$this->mssql=$mssql;
		$this->mssql->debug=false;
		$this->comtrex=$comtrex;
		$this->micros=$micros;
		$this->share=$share;
		$this->user=$user;
	}

	function get_content($p,$type) {
		#BUILD CONTENT OF PAGE
		$page_content=$this->share->page_defaults();
		$page_content=$this->get_page($p,$type);
		$seo=$this->share->keywords($page_content['title']." ".$page_content['content']);
		$page_content['ssl']=$page_content['ssl'];
		$page_content['keywords']=$seo['keywords'];
		$page_content['description']=$seo['desc'];
		$page_content['content']=str_replace("%%SERVER%%",$p,$page_content['content']);
		return $page_content;
	}

	private function get_page($p,$type) {
		$p=str_replace("/","",$p);
		$page_content=NULL;
		$p=strtolower($p);
		if(empty($p)) {
			$p='index';
		} elseif($p=='logout') {
			session_destroy();
			session_start();
			$p='index';
		}
		if($p=='points' && empty($_SESSION['user_id'])) {
			$p='index';
		}
		switch($type) {
			default:
				$page_content=$this->share->view_page(TEMPLATES.$p);
			break;
		}
		switch(strtolower($p)) {
			case "register":
				$fields=$this->db->show_columns('fan_club_users');
				$profile=file_get_contents(TEMPLATES."profile.html");
				foreach($fields as $k) {
					switch($k) {
						case "store_id":
							$value=$this->share->stores('option');
						break;
						default:
							$value=NULL;
						break;
					}
					$profile=str_replace("%%".$k."%%",$value,$profile);
				}
				$profile=str_replace("%%card_number%%",file_get_contents(TEMPLATES."card_number.html"),$profile);
				$profile=str_replace("%%status%%","create",$profile);
				$page_content['content']=str_replace("%%profile%%",$profile,$page_content['content']);
				$page_content['bottom_java']=str_replace("%%today%%",date("m/d/Y"),$page_content['bottom_java']);
			break;
			case "points":
				$page_content['headertitle']=str_replace("%%name%%",$_SESSION['fname'],$page_content['headertitle']);
/*				$page_content['headertext']=$this->user->list_card_details();*/
				$page_content['content']=$this->user->profile_page($page_content['content']);
			break;
			case "account":
				$page_content['headertitle']=str_replace("%%name%%",$_SESSION['fname'],$page_content['headertitle']);
				$page_content['content']=$this->user->get_user_pages($_SESSION['user_id']);
			break;
		}
		return $page_content;
	}

}
?>