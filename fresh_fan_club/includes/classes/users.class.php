<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
MAIN CLASS all other classes are extensions of this primary class

This php file was refactored and annotated by Anson Han, Star Group December 2013
/-------------------------------------------------------*/

/* to enable php debugging, uncomment the next two lines of code*/
/*
ini_set('display_errors',1);
error_reporting(E_ALL);
*/



/* check to see if page exists */
if(!empty($_SERVER['REQUEST_URI'])) {
	$requestedURL = parse_url($_SERVER['REQUEST_URI']);
	if(basename($requestedURL['path']) === basename(__FILE__)) {
		header("location: /page-not-found");
	}
}


class USERS {

	function __construct(&$db,&$mssql,&$comtrex,&$micros,&$share) {
		$this->db=$db;
		$this->db->debug=false;
		$this->mssql=$mssql;
		$this->mssql->debug=false;
		$this->micros=$micros;
		$this->comtrex=$comtrex;
		$this->share=$share;
	}


/***
*  @function fan_club_login()
*  description: processes login and returns matching fan_club_user from mysql db fan_club_users table
*/
	public function fan_club_login() {
		#User Login
		$return=NULL;
		$email=strtolower(trim($_POST['email']));

		$pass=$this->share->stone(trim($_POST['password']));

		/********************************************************
		 * This is from SWcom/fresh_fan_club2/includes/key.inc.php
		 * Changes there require changes here to work
		 *******************************************************/
		#Rosetta Security key
		#!!!!!DO NOT CHANGE. CHANGING WILL RENDER PASSWORDS UNRECOVERABLE
		define('SECURITY_PHRASE','Eat Harvest Fresh Salads');
		define('SECURITY_KEY',' You will turn green');
		define('SECURITY_LTRS','ABCDEFGH');
		define('SECURITY_STYLE','57569278');
		/*******************************************************/
		$encrypted1 = mcrypt_cbc(MCRYPT_BLOWFISH, SECURITY_PHRASE, trim($_POST['password']), MCRYPT_ENCRYPT, SECURITY_STYLE);
		$encrypted2 = mcrypt_cbc(MCRYPT_TRIPLEDES, SECURITY_KEY, $encrypted1, MCRYPT_ENCRYPT, SECURITY_LTRS);
		$pass = base64_encode($encrypted2);


		$sql = "select * from fan_club_users where lower(email)='$email' and password='$pass'";
		$this->db->query($sql);
		if($this->db->num_rows() != 0) {
			$r=$this->db->movenext();
			$_SESSION['user_id']=$r['user_id'];
			$_SESSION['fname']=$r['first_name'];
			$_SESSION['lname']=$r['last_name'];
			/*$start=strtotime("now");
			$stop=strtotime($r['cache_check']);
			$difference=$start-$stop;
			$hours = round($difference/SECONDS_PER_HOUR,0);
			if($hours > 6) { //Check for Points Update after 6 hours of last check
			}*/
			$this->fan_club_points_update($r['user_id']);
			$this->db->update("update fan_club_users set last_login=NOW() where user_id=".(int)$r['user_id']);
			$return=$this->get_user_pages($r['user_id']);
		} else {
			$return='false^^Invalid Email or Password';
		}
		return $return;
	}
/*
*  end fan_club_login()
***/


/***
*  @function fan_club_register()
*  description: processes new fan_club_user registration to mysql db fan_club_users table
*/
	public function fan_club_register() {
		#Register User
		$return=NULL;
		$ignore=array('user_id','points','create_date','last_login','cache_check','subscription');
		$fields=$this->db->show_columns('fan_club_users');
		foreach($fields as $f) {
			if(!in_array($f,$ignore)) {
				switch($f) {
					case "password":
						$value=$this->share->stone($_POST[$f]);
					break;
					default:
						if($f == 'cell_phone')
						{
							$phone = $value = preg_replace('/\D/', '', $_POST[$f]);
						}
						else
						{
							$value=$_POST[$f];
						}
					break;
				}
				$insert[]=$f;
				$insert_value[]="'".$this->db->clean($value)."'";
				$update[]=$f."='".$this->db->clean($value)."'";
				$$f=$value;
			}
		}

		$card_number = preg_replace('/\D/', '', $_POST['card_number']);

    /* check to see if user is already registered */
		if(!empty($_SESSION['user_id']))
		{
			$this->db->query("Select email from fan_club_users where lower(email)='$email' AND user_id != $_SESSION[user_id]");

			if($this->db->num_rows() == 0)
			{
				if($phone)
				{
					$this->db->query("Select cell_phone from fan_club_users where cell_phone ='$phone' AND user_id != $_SESSION[user_id]");

					if($this->db->num_rows() == 0)
					{
						$update[]="subscription='".$_POST['subscription']."'";
						$this->db->update("update fan_club_users set ".implode(',',$update)." where user_id=".(int)$_SESSION['user_id']);
						$return=$this->get_user_pages($_SESSION['user_id']);
					}
					else
					{
					 	$return="false^^This phone number is already in use...\n\nPlease choose a different phone number \n\nOR\n\n if this is your phone number, log into the account associated with this number";
					}
				}
				else
				{
				 	$return="false^^Please enter a phone number to use should you forget your card at the store";
				}
			}
			else
			{
				$return="false^^This email is already in use..\nPlease Logon or Enter your email in the Forgot Logon window to get your password";
			}
		}
		/* no existing user found with same criteria, Register New Profile */
		else
		{
			$this->db->query("Select email from fan_club_users where email = '$email'");

			if($this->db->num_rows() == 0)
			{

				if($phone)
				{
					$this->db->query("Select cell_phone from fan_club_users where cell_phone ='$phone'");

					if($this->db->num_rows() == 0)
					{
						$sql="insert into fan_club_users (".implode(',',$insert).",points,create_date,last_login) ";
						$sql.="values ";
						$sql.="(".implode(',',$insert_value).",0,NOW(),NOW())";
						$user_id=$this->db->insert($sql);
						$_SESSION['user_id']=$user_id;
						$_SESSION['fname']=$first_name;
						$_SESSION['lname']=$last_name;

		//$return = "false^^Trying Create-Harvest with $card_number\n";
						$success = $this->createIfNotExistInHarvest($card_number);
		//$return .= "Response: $success\n";

						$t_points=$this->add_fan_club_card($card_number);

		//$return .= "Add Card to sw: t_points: $t_points\n";
						if(!empty($t_points))
						{
							$this->db->update("update fan_club_users set points=$t_points, cache_check=NOW() where user_id=".(int)$user_id);
						}

						$notice=$this->share->read_file(TEMPLATES.'email.html');
						foreach($_POST as $k => $v) {
							$notice=str_replace("%%".$k."%%",$v,$notice);
						}
						$message['subject']="Your Fresh Fan Club Registration is Complete";
						$message['email']=$email;
						$message['form']=$notice;
						$alt=str_replace(array("<br>","<br />","</p>","</div>"),"\r\n",$notice);
						$message['alt_form']=strip_tags($alt);
						$this->share->send_email($message);
						$this->get_user_pages($user_id);
					}
					else
					{
					 	$return="false^^This phone number is already in use...\n\nPlease choose a different phone number \n\nOR\n\n if this is your phone number, log into the account associated with this number";
					}
				}
				else
				{
				 	$return="false^^Please enter a phone number to use should you forget your card at the store";
				}
			}
			else
			{
				$return="false^^This email is already in use..\nPlease Logon or Enter your email in the Forgot Logon window to get your password";
			}
		}

		return $return;
	}
/*
*  end fan_club_register()
***/



/***
*  @function profile_page()
*  description: processes fan_club_user Points board/signed-in splash page
*/
	public function profile_page($content) {
		$points = 0;
		$cards = $this->temp_list_card_details();
    $cardPointSummary = $this->list_card_details();
		if(!empty($cards)) {
			foreach($cards as $c) {
				$points=$points+$c['points'];
			}
		}

		// hack since old number displays page until refresh after fancode redirect
		if(isset($_REQUEST['fancodepoints']))
		{
			$points = $_REQUEST['fancodepoints'];
		}

		$this->fan_club_settle_points($_SESSION['user_id']);


		$fanpointsform = '<div class="fancodeform">
		<form action="/fresh_fan_club2/fancode.php" method="post">
		<a href="https://www.facebook.com/saladworks" target="_blank"><span style="color:#fff;font-weight:normal">got a fan code?</span></a> <input name="fancode" id="fancode" size="10"> <input type="submit" name="enter code" value="enter code">
		<input type="hidden" name="userid" id="userid" value="%%userid%%">
		</form>
		</div>
		<div class="fancodeformmsg">%%fancodemessage%%</div>';

/*
		$selfServeFFpoints = '<div style="text-align:center;font-size:16px;margin:6px">forgot your card?  <a href="receiptpoints.php">click here to add points</a></div>';

		$selfServeFFpoints = '<div style="text-align:center;font-size:16px;margin:6px;background-color:#ffffff;"><a href="faq.html#online"><span id="forgotcard" style="color:#6639B7;" onmouseover="this.style.backgroundColor=\'#F79229\';this.style.color=\'#ffffff\';" onmouseout="this.style.backgroundColor=\'#ffffff\';this.style.color=\'#6639B7\';"><b>Missed points can no longer be added online, click here for further details.</b></span></a></div><br>';
*/

		/****************************************************
		 * Comment out next line TO SHOW add your own points
		 ****************************************************/
		$selfServeFFpoints = '';


		/****************************************************
		 * Comment out next line TO SHOW the fan points form
		 ****************************************************/
		$fanpointsform = '';



		$content = str_replace("%%fanpointscode%%", $fanpointsform, $content);
		$content = str_replace("%%selfserveaddffpoints%%", $selfServeFFpoints, $content);

		$content = str_replace("%%points%%",$points,$content);
		$content = str_replace("%%cards%%",$cardPointSummary,$content);
		$content = str_replace("%%userid%%",$_SESSION['user_id'],$content);
		$content = str_replace("%%fancodemessage%%",urldecode($_REQUEST['fancodemessage']),$content);
		setcookie ("fancodemessage", "", time() - 3600);

		return $content;
	}
/*
*  end profile_page()
***/



/***
*  @function get_user_pages()
*  description: router for User Account Management page (require users to be logged in)
*/
	public function get_user_pages($user_id) {
		$data=file_get_contents(TEMPLATES."content.html");
		$profile=file_get_contents(TEMPLATES."profile.html");
		$r=$this->share->get_fan_club_user($user_id);
		foreach($r as $k=>$v) {
			switch($k) {
				case "password":
					$value=$this->share->sober($v);
				break;
				case "subscription":
					$value=file_get_contents(TEMPLATES."subscription.html");
					$x="<option value=\"1\"".($v==1?" selected=\"selected\"":NULL).">Yes</option>";
					$x.="<option value=\"2\"".($v==2?" selected=\"selected\"":NULL).">No</option>";
					$value=str_replace("%%subscription%%",$x,$value);
				break;
				case "store_id":
					$stores=$this->share->stores();
					$value=NULL;
					foreach($stores as $s) {
						$value.="<option value=\"".$s['store_no']."\"".($v==$s['store_no']?" selected=\"selected\"":NULL).">".$s['name']."</option>";
					}
				break;
				default:
					$value=$v;
				break;
			}
			$profile=str_replace("%%".$k."%%",$value,$profile);
		}
		$profile=str_replace("%%status%%","Update",$profile);
		$profile=str_replace("%%card_number%%",NULL,$profile);
		$data=str_replace("%%cards%%",$this->list_card_details(),$data);
		$data=str_replace("%%profile%%",$profile,$data);
		return $data;
	}
/*
*  end get_user_pages()
***/


	private function temp_list_card_details() {
		$cards=NULL;
		$this->db->query("select * from fan_club_users_cards where user_id=".(int)$_SESSION['user_id']);
		if($this->db->num_rows() != 0) {
			$cards = $this->db->get_records();
		}
		return $cards;
	}


	public function list_card_details() {
		$cards = '';
		$this->db->query("select * from fan_club_users_cards where user_id=".(int)$_SESSION['user_id']);
		if($this->db->num_rows() != 0)
		{
      $cards .= '<ul>' . "\n";
			while($r=$this->db->movenext())
			{
				$cardNum = preg_replace('/\D/','',$r['loyalty_card']);
				$len = strlen($cardNum);
				$cFormatted = '';
				for($i = 0; $i < $len; $i++)
				{
					$cFormatted .= substr($cardNum,$i,1);
					if($i < $len - 1 && $i % 4 == 3) // chunk in 4s: 1234-1234-12
					{
						$cFormatted .= '-';
					}
				}

				$cards.= '<li>' . $cFormatted . ':&nbsp;' . $r['points'] . ' points</li>' . "\n";
			}
      $cards .= '</ul>' . "\n";
		}

		// hack since old number displays page until refresh after fancode redirect
		if(isset($_REQUEST['fancodecardpoints']))
		{
			$points = $_REQUEST['fancodecardpoints'];
		}


		return $cards;
	}


	public function update_profile_cards($card_number) {
		$t_points=$this->add_fan_club_card($card_number);
		if(!empty($t_points)) {
			$this->fan_club_settle_points($_SESSION['user_id']);
			$return=$this->get_user_pages($_SESSION['user_id']);
		} else {
			$return="false^^Invalid Card Number";
		}
		return $return;
	}


	public function fan_club_points_update($user_id)
	{

		$this->fan_club_settle_points($user_id);
	}


	private function fan_club_settle_points($user_id)
	{
		$this->updatePointsFromHarvest($user_id);
		$this->db->update("update fan_club_users set points=(select SUM(points) from fan_club_users_cards where user_id=".(int)$user_id."), cache_check=NOW() where user_id=".(int)$user_id);
	}


/***
*  @function updatePointsFromHarvest()
*  description: retrieve fanclub point balance from P.O.S. msSQL db and sync/update mySQL db
*/
	public function updatePointsFromHarvest($user_id)
	{
    include("db_settings.php");
		// Local MySQL conn
		$link = mysql_connect($mySQLhost, $mySQLuser, $mySQLpass);
		$db=mysql_select_db($mySQLdb, $link);
    unset ($mySQLdb, $mySQLhost, $mySQLuser, $mySQLpass);


		// Harvest conn
		$mslink = mssql_connect($msSQLhost,$msSQLuser,$msSQLpass);
		mssql_select_db($msSQLdb, $mslink);
    unset ($msSQLdb, $msSQLhost, $msSQLuser, $msSQLpass);


		$sql = "select loyalty_card from fan_club_users_cards where user_id=".(int)$user_id;
		$result = mysql_query($sql,$link) or die(mysql_error());

		if(mysql_num_rows($result) > 0)
		{
			$addcomma = 0;
			$cards = '';

			while ($row = mysql_fetch_assoc($result))
			{
				$lc = str_replace('-','',$row['loyalty_card']);
				if($addcomma)
				{
					$cards .= ',';
				}
				$cards .= "'$lc'";
				$addcomma++;
			}

			// get card/points from harvest
			$mssql = "SELECT AccountNumber, Points FROM Accounts WHERE AccountNumber IN ($cards) ;";
			$res = mssql_query ( $mssql, $mslink );

			while ($msrow = mssql_fetch_assoc($res))
			{
				$sql = "update fan_club_users_cards set points = $msrow[Points] where user_id = $user_id and loyalty_card = '$lc';";
				$success = mysql_query($sql,$link) or die(mysql_error());
			}

			// shut down, go home
			mssql_close($mslink);
			mysql_close($link);
		}

	}
/*
*  end updatePointsFromHarvest()
***/


	public function add_fan_club_card($card_number)
	{

		$this->createIfNotExistInHarvest($card_number);

		$return=NULL;
		$registerPoints = 0;

		$this->db->query("select loyalty_card from fan_club_users_cards where REPLACE(loyalty_card,'-','')='".str_replace("-","",$card_number)."'");

		if($this->db->num_rows() == 0) // First time give 200 points
		{
			$registerPoints = 200;
		}

		$array=array('merchant_id'=>DEFAULT_MERCHANT,'code'=>'Add','invoice'=>NULL,'ref'=>NULL,'card'=>$card_number,'purchase'=>NULL,'points'=>$registerPoints,'units'=>NULL,'price'=>NULL,'promo_id'=>NULL);
		$t_points=$this->quick_points_report($array);

		if(!preg_match('/[Invalid Account Number]/',$t_points))
		{
			$sql = "insert into fan_club_users_cards (user_id,loyalty_card,points) values (".$_SESSION['user_id'].",'$card_number',$t_points) ON DUPLICATE KEY UPDATE points=$t_points";
			$this->db->update($sql);
			$return=$t_points;
		}

		return $return;
	}


	public function quick_points_report($array) {
		#Login and Registration Quick Update
		$details['merchant_id']=$array['merchant_id'];
		$details['code']=$array['code'];//'Balance';#Add,Subtract,VoidAdd,Balance
		$details['invoice']=$array['invoice'];
		$details['ref']=$array['ref'];
		$details['account_number']=str_replace("-","",$array['card']);//"7023084793630180390";
		$details['purchase']=$array['purchase'];//62.90;
		$details['points']=$array['points'];//round(62.90);
		$details['units']=$array['units'];//2;
		$details['price']=$array['price'];//35.25;
		$details['promo_id']=$array['promo_id'];//1189;
		$res=$this->comtrex->contact_comtrex($details);

		if($res['CmdResponse']->CmdStatus=='Approved') {
			$points=$res['TranResponse']->Items->PointsBalance;
		} else {
			$points=$res['CmdResponse']->TextResponse;
		}
		return $points;
	}


	public function forgot_logon() {
		if(!empty($_POST['email'])) {
			$return="false^^The email was not found in our system\nPlease try again.";
			$email=trim(strtolower($_POST['email']));
			if(!empty($email)) {
				$this->db->query("select * from fan_club_users where lower(email)='$email'");
				if($this->db->num_rows()!=0) {
					$r=$this->db->movenext();
					$password=$this->share->sober($r['password']);
					$notice=file_get_contents(TEMPLATES."notice_reminder.html");
					$notice=str_replace("%%name%%",$r['first_name']." ".$r['last_name'],$notice);
					$notice=str_replace("%%email%%",$r['email'],$notice);
					$notice=str_replace("%%password%%",$password,$notice);
					$notice=str_replace("%%HOST%%",WEBSITE,$notice);
					$msg['form']=$notice;
					$alt=str_replace(array("<br />","</p>"),"\r\n",$notice);
					$msg['alt_form']=strip_tags($alt);
					$msg['email']=$r['email'];
					$msg['name']=$r['first_name']." ".$r['last_name'];
					$msg['subject']="Your reminder for the SaladWorks Fresh Fan Club";
					$this->share->send_email($msg);
					$return="true^^An email has been sent to " . $email . "<br><br>While it should arrive very quickly, please allow upwards of an hour for it arrive";
				}
			}
		} else {
			$return=file_get_contents(TEMPLATES."reminder.html");
		}
		return $return;
	}


	public function createIfNotExistInHarvest($cardNum)
	{
		$cardNum = preg_replace('/\D/', '', $cardNum);

		$mslink = mssql_connect('66.187.149.119','salad_web','Seminol9989');
		mssql_select_db('Loyalty', $mslink);

		$mssql = "SELECT COUNT(*) AS exist FROM Accounts WHERE AccountNumber = '$cardNum'";

		$result = mssql_query( $mssql, $mslink );

		$row = mssql_fetch_assoc($result);

		if($row['exist'])
		{
			return $r . "AccountExists\n";
			return 1;
		}
		else
		{
			// Create Account
			$mssql = "INSERT INTO [Accounts]
						([Hash]
						,[AccountNumber]
						,[BrandID]
						,[Active]
						,[LostStolen]
						,[Points]
						,[Amount]
						,[SignupPointsAllowed]
						,[SignupPointsApplied]
						,[SignupPointsCallbackCode]
						,[LastTransactionDateTime]
						,[LastTransactionMerchantID]
						,[Status]
						,[LastTransactionLocalTimeOffset])
					VALUES
						(NULL
						,'$cardNum'
						,2
						,1
						,0
						,0
						,0.00
						,0
						,0
						,'MICROSCMC'
						,GETDATE()
						,(SELECT MerchantID FROM Merchants WHERE MerchantPOSID = '488888')
						,''
						,-5)";
			$result = mssql_query( $mssql, $mslink );

			$success = $this->addToTransactionLog('','Create',0,'0.00',0,'0.00',0,'0.00',$cardNum,'488888',$mslink);

			return $success;
		}
	}


	public function addToTransactionLog($refNo, $tranCode, $points, $amount, $addPoints, $addAmount, $redeemPoints,$redeemAmount,$cardNum,$posID,&$mslink)
	{

			$mssql = "INSERT INTO [TransactionLog]
			   ([AccountID]
			   ,[MerchantID]
			   ,[AccountNumber]
			   ,[MerchantPOSID]
			   ,[RefNo]
			   ,[TranCode]
			   ,[Points]
			   ,[Amount]
			   ,[PointsBalance]
			   ,[TransactionDateTime]
			   ,[ReturnCode]
			   ,[AddPoints]
			   ,[AddAmount]
			   ,[RedeemPoints]
			   ,[RedeemAmount]
			   ,[TransactionLocalTimeOffset])
		 VALUES
			   ((SELECT AccountID FROM Accounts WHERE Accounts.AccountNumber = '$cardNum')
			   ,(SELECT MerchantID FROM Merchants WHERE MerchantPOSID = '488888')
			   ,'$cardNum'
			   ,'$posID'
			   ,'$refNo'
			   ,'$tranCode'
			   ,$points
			   ,$amount
			   ,(SELECT Points from Accounts WHERE AccountNumber = '$cardNum')
			   ,GETDATE()
			   ,'00000'
			   ,$addPoints
			   ,$addAmount
			   ,$redeemPoints
			   ,$redeemAmount
			   ,0);";


			$result = mssql_query( $mssql, $mslink );

			if($result == FALSE)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}

	}


} /* end class USERS */
?>