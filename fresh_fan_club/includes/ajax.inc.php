<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
/-------------------------------------------------------*/
require_once("key.inc.php");
$content='false^^No Information Found';
if(!empty($_REQUEST['ajax'])) {
	switch($_REQUEST['ajax']) {
		case "fan_club_register":
			$content=$user->fan_club_register();
		break;
		case "fan_club_login":
			$content=$user->fan_club_login();
		break;
		case "add_loyalty_card":
			$content=$user->update_profile_cards($_POST['card_number']);
		break;
		case "terms_conditions":
			$content=file_get_contents(TEMPLATES."terms.html");
			$content=str_replace("%%date%%",date("F j, Y"),$content);
			$content=str_replace("%%year%%",date("Y"),$content);
		break;
		case "forgot_logon":
			$content=$user->forgot_logon();
		break;
		case "add_loyalty_card":
			$content=$user->update_profile_cards($_POST['card_number']);
		break;
	}
}
echo $content;
exit;
?>
