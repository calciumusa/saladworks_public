<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
Primary Home Page for Loyalty Program
/-------------------------------------------------------*/


require_once("includes/key.inc.php");
require_once("includes/classes/site.class.php");
$site=new SITE($db,$mssql,$comtrex,$micros,$share,$user);
$p=(!empty($_GET['page'])?strtolower($_GET['page']):'index');
$t=(!empty($_GET['type'])?strtolower($_GET['type']):'content');
$content=$site->get_content($p,$t);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $content['title'];?></title>

<?php include("../pseudo-templates/head-includes.php"); ?>

<meta name="resource-type" content="document" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="revisit-after" content="3 days" />
<meta name="classification" content="Internet" />
<meta name="Description" content="<?php echo $content['description'];?>" />
<meta name="Keywords" content="<?php echo $content['keywords'];?>" />
<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<meta name="robots" content="ALL" />
<meta name="robots" content="index, follow" />
<meta name="distribution" content="Global" />
<meta name="rating" content="Safe For Kids" />
<meta name="copyright" content="Copyright <?php echo date("Y");?> Saladworks  http://www.saladworks.com" />
<meta http-equiv="Reply-to" content="marketing@saladworks.com" />
<meta name="language" content="English" />

<link rel="stylesheet" type="text/css" href="/fresh_fan_club2/includes/css/saladworks-new.css" />
<link rel="stylesheet" type="text/css" href="/fresh_fan_club2/includes/css/loyalty.css"/>
<link rel="stylesheet" type="text/css" href="/fresh_fan_club2/includes/css/sw-freshfanclub.css"/>

<script type="text/javascript" src="/fresh_fan_club2/includes/js/jquery.js"></script>
<script type="text/javascript" src="/fresh_fan_club2/includes/js/loyalty.js"></script>


<?php echo $content['top_java'];?>
</head>

<body class="not-front no-sidebars node-type-page">
<?php include("../pseudo-templates/static-header.php"); ?>

<div class="page">
<h1 class="page-title">Fresh Fan Club</h1>
<div role="main" id="main-content">
<article class="node node-page " about="/fresh_fan_club2" typeof="foaf:Document" role="article">
<div id="main">
    <div id="main_header">
      <div class="background">
        <div class="headertag"><?php echo $content['headertitle'];?></div>
      </div>
      <div class="headermsg">
        <?php echo $content['headertext'];?>
      </div>
    </div>
    <div id="policy_dialog" style="font-size:12px"></div>

    <div id="loyalty_content">

      <div id="testingOnly" style="display:none;visibility:hidden;">
        <h1>This is where the content would be populated.</h1>
        <p>Click a link below to preview the static test page for layout:</p>
        <ol>
          <li><a href="1landing.php">Sample Landing Page</a></li>
          <li><a href="2signup.php">Sample Signup Page</a></li>
          <li><a href="3pointsboard.php">Sample Returning User Splash Page</a></li>
          <li><a href="4account.php">Sample Existing User Modify Account Page</a></li>
          <li>Modal Popups
            <ol>
              <li><a href="includes/templates/reminder.html">Forgot Your Username/Password</a></li>
              <li><a href="includes/templates/terms.html">Terms & Conditions</a></li>
            </ol>
          </li>
          <li>Relocated Pages
            <ol>
              <li><a href="/fresh-fan-club-frequently-asked-questions">Fresh Fan Club FAQs</a></li>
            </ol>
          </li>
        </ol>
      </div><!--/#testingOnly-->

      <?php echo $content['content'];?>
    </div><!--/#loyalty_content-->

  </div><!--/#main-->
  </article>
  </div><!--/#main-content-->
</div><!--/.page-->

<?php include("../pseudo-templates/static-footer.php"); ?>

<?php echo $content['bottom_java'];?>
</body>
</html>
