<?php
/*------------------------------------------------------/
Development By:
Strategic Internet Marketing & Management, LLC.
Bob Babb (Founder)
bob@simmllc.com
215-853-2623
# Micros Web Service
/-------------------------------------------------------*/
require_once("includes/key.inc.php");
require_once("includes/classes/web_service.class.php");
$web=new WEB_SERVICE($db,$mssql,$comtrex,$micros,$share);
$content=NULL;
if(!empty($_REQUEST['store_no']) & !empty($_REQUEST['q'])) {
	$content=$web->switch_post();
	if($content != 'error') {
		echo $content;
		//exit;
	} else {
		header("location: /UNABLE_TO_FIND");
	}
} else {
	header("location: /UNABLE_TO_FIND");
}
?>