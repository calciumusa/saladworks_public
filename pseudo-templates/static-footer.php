<footer id="global-footer" class="footer">
  <div id="inner-footer" class="inner-footer" role="contentinfo">


<div class="footer-about ">

        <h2 class="title">About Saladworks</h2>


  <p>Saladworks is the nation's first and largest fresh-tossed salad franchise concept, with over 100 locations across the country. Saladworks offers a "fanatically fresh" menu of America's Best Salads with signature dressings, proprietary soups, and Focaccia Fusion sandwiches. All Saladworks salads are made-to-order, chopped on-location, and assembled right in front of your eyes. With the addition of the True Nutrition menu, Saladworks' varieties of signature salads are all 500 calories or less.</p>

  </div>

<nav class="footer-menu " role="navigation">


  <ul class="main-menu"><li><a href="https://online.saladworks.com/#/pickup">Find your closest Saladworks</a></li>
<li><a href="/salad">Browse our menu</a></li>
<li><a href="https://online.saladworks.com">Order online for pickup or delivery</a></li>
<li><a href="http://www.link.com">Learn about our Fresh Fan program</a></li>
</ul>






</nav>

<div class="footer-social ">



  <div class="social-links">
<ul><li><a class="fb-white" href="https://www.facebook.com/saladworks/" target="_blank"></a><a href="https://www.facebook.com/saladworks/" target="_blank">Like on Facebook</a></li>
<li><a class="twit-white" href="https://www.twitter.com/saladworks/" target="_blank"></a><a href="https://www.twitter.com/saladworks/" target="_blank">Follow on Twitter</a></li>
<li><a class="pin-white" href="http://www.pintrest.com/saladworks/" target="_blank"></a><a href="http://www.pinterest.com/saladworks/" target="_blank">Find Us on Pintrest</a></li>
</ul></div>
<p>&nbsp;</p>

  </div>

<div class="footer-franchise-ops ">



  <p><a href="http://saladworksfranchises.com" target="_blank"><img class="glowing-salad-img" src="/sites/all/themes/beta_carrotheme/img/open-sign.png"></a></p>
<h2>
	<a href="http://saladworksfranchises.com" target="_blank">Franchise Opportunities</a></h2>
<h4>
	<a href="http://saladworksfranchises.com" target="_blank">saladworksfranchises.com</a></h4>

  </div>

<div class="footer-copyright ">



  <p>©2013 Copyright Saladworks.com. All Rights Reserved.</p>

  </div>

  </div>
</footer>