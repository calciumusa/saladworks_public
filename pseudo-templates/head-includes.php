<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="http://saladworks.stargroupdev.com/sites/all/themes/beta_carrotheme/favicon.ico">
<meta name="Generator" content="Drupal Mothership">
<link rel="shortlink" href="http://saladworks.stargroupdev.com/node/27">
<meta name="generator" content="Drupal 7 (http://drupal.org)">
<meta content="Salads" about="/salad" property="dc:title">
<meta about="/salad" property="sioc:num_replies" content="0" datatype="xsd:integer">
<link rel="canonical" href="http://saladworks.stargroupdev.com/salad">
<link rel="apple-touch-icon" sizes="144x144" href="http://saladworks.stargroupdev.com/sites/all/themes/beta_carrotheme/apple-touch-icon-144x144.png"><link rel="apple-touch-icon" sizes="114x114" href="http://saladworks.stargroupdev.com/sites/all/themes/beta_carrotheme/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="http://saladworks.stargroupdev.com/sites/all/themes/beta_carrotheme/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" href="http://saladworks.stargroupdev.com/sites/all/themes/beta_carrotheme/apple-touch-icon.png">
<link rel="apple-touch-startup-image" href="http://saladworks.stargroupdev.com/sites/all/themes/beta_carrotheme/apple-startup.png">
<meta name="MobileOptimized" content="width">
<meta name="HandheldFriendly" content="true"><meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="cleartype" content="on">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<style>@import url("http://saladworks.stargroupdev.com/modules/system/system.base.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/modules/system/system.menus.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/modules/system/system.messages.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/modules/system/system.theme.css?mx8oqa");</style>
<style>@import url("http://saladworks.stargroupdev.com/modules/comment/comment.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/sites/all/modules/date/date_api/date.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/modules/field/theme/field.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/modules/node/node.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/modules/search/search.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/modules/user/user.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/sites/all/modules/views/css/views.css?mx8oqa");</style>
<style>@import url("http://saladworks.stargroupdev.com/sites/all/modules/ckeditor/ckeditor.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/sites/all/modules/ctools/css/ctools.css?mx8oqa");</style>
<style>@import url("http://saladworks.stargroupdev.com/sites/all/themes/mothership/mothership/css/normalize.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/sites/all/themes/mothership/mothership/css/mothership-default.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/sites/all/themes/mothership/mothership/css/mothership-layout.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/sites/all/themes/mothership/mothership/css/mothership.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/sites/all/themes/beta_carrotheme/css/style.css?mx8oqa");
@import url("http://saladworks.stargroupdev.com/sites/all/themes/mothership/mothership/css/mothership-devel-mediaqueries.css?mx8oqa");</style>
<!--[if lt IE 9]>
  <script src="http://saladworks.stargroupdev.com/sites/all/themes/mothership/mothership/js/respond.min.js"></script>
<![endif]-->
<!--[if lt IE 9]>
  <script src="http://saladworks.stargroupdev.com/sites/all/themes/mothership/mothership/js/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="//use.typekit.net/iyr7qsm.js"></script>
<style type="text/css">.tk-museo-sans,museo-sans{font-family:"museo-sans",sans-serif;}</style><link rel="stylesheet" href="http://use.typekit.net/c/697fe7/museo-sans:i1:i3:i5:i7:i9:n1:n3:n5:n7:n9.Py9:N:2,PyB:N:2,PyD:N:2,PyG:N:2,PyJ:N:2,Py8:N:2,Py6:N:2,PyC:N:2,PyF:N:2,PyH:N:2/d?3bb2a6e53c9684ffdc9a98ff135b2a6203cee65698212e0e40cde6bbc15c93fd2e89faebdf64cdba4738eed4a3c55f769a5251e7a78a721b49a1e4454dd222a84ff6018eb063b6628afcf5224f3ec4113a98f7383e926bb3fb66bd57d9f9753a2fc10c98f70bb3185a1f1e843fb093d9a941abd7d824d55d29d2bde6c658ab1943549d09567a1a1ef45732d8cfd8e7e8a07914c5f91acc8693d3c0a0a35c27e9ad2267c61e5b6e3465bd4be610ec72885d7544124a5296789108adae47b57a97018098d234d0fd729fcb1222f16761aaa6915e7952c23e2badd5383cfb2a90c1e9d0d78072520f18e92cab225d5ce9063e12d456f067c7798e38bc93808ecc12a2dba6e9bc85099d415b55abe73348bf8368d79b8a080bf1957702004ef8118ab551234ec2bc3ec901053e"><script type="text/javascript">try{Typekit.load();}catch(e){}</script>
